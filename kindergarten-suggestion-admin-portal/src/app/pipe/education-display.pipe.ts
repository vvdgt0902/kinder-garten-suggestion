import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'educationDisplay',
  standalone: true
})
export class EducationDisplayPipe implements PipeTransform {

  transform(value: string): string {

    switch (value) {
      case 'MONTESSORI':
        return "Montessori";
      case 'STEM':
        return "Stem";
      case 'STEINER':
        return "Steiner";
      case 'REGGIO_EMILIA':
        return "Reggio Emilia";
      case 'HIGH_SCOPE':
        return "High Scope";
      case 'SHICHIDA':
        return "Shichida";
      case 'GLENN_DOMAN':
        return "Glenn Doman";
      default:
        return value;
    }
  }

}
