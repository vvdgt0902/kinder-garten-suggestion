import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { environment } from "../../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private _httpClient: HttpClient) { }

  getUserList(page: number, size: number, searchOptions: any): Observable<any> {
    let params = new HttpParams()
      .set('page', page)
      .set('size', size);

    if (searchOptions) {
      params = params.set('q', searchOptions);
    }
    return this._httpClient.get(`${environment.apiPrefixUrl}/admin/user/list`, {
      params
    });
  }
  delete(id: number): Observable<any> {
    return this._httpClient.delete(`${environment.apiPrefixUrl}/admin/user/deleted/${id}`);
  }

  getUserDetailById(id: number): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/admin/user/${id}`);
  }
  updateUserStatus(id: number): Observable<any> {
    return this._httpClient.put(`${environment.apiPrefixUrl}/admin/user/updateStatus/${id}`, {});
  }

  getAllOwnerSchool():Observable<any>{
    return this._httpClient.get(`http://localhost:8686/api/admin/user/owner`);
  }

}
