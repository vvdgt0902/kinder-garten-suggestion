import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestService {


  constructor(private _httpClient: HttpClient) { }
  getRequestList(page:number,size: number,searchOptions: any): Observable<any> {
    let params = new HttpParams()
      .set('page', page)
      .set('size', size);

    if (searchOptions) {
      params = params.set('q', searchOptions);
    }
    return this._httpClient.get(`${environment.apiPrefixUrl}/admin/request/list`, {
      params
    });
  }
  getRequestDetailById(id: number): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/admin/request/counsellingDetail/${id}`);
  }
  updateStatusRequest(id:number): Observable<any> {
    return this._httpClient.put(`${environment.apiPrefixUrl}/admin/request/updateStatusCounselling/${id}`, {});
  }
  getReminderRequest(page: number, size: number, searchKeywordOpt : any):Observable<any>{
    let params = new HttpParams()
      .set('page', page)
      .set('size', size);

    if(searchKeywordOpt){
      params = params.set('q',searchKeywordOpt);
    }
    return this._httpClient.get(`${environment.apiPrefixUrl}/admin/request/listReminder`,{params})
  }
}
