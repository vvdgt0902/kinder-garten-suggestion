import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, UntypedFormGroup, Validators} from "@angular/forms";

import {SchoolService} from "../../../../service/school-service/school.service";
import {finalize} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";

import {NzNotificationService} from "ng-zorro-antd/notification";
import {MailService} from "../../../../service/mail-service/mail.service";
import {AdminSchoolService} from "../../../../service/admin-school-service/admin-school.service";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {AuthService} from "../../../../service/auth-service/auth.service";
import {UserService} from "../../../../service/user-service/user.service";

@Component({
  selector: 'app-view-crud-school',
  templateUrl: './view-crud-school.component.html',
  styleUrls: ['./view-crud-school.component.scss']
})

export class ViewCrudSchoolComponent implements OnInit, AfterViewInit {
  size: 'small' | 'middle' | 'large' | number = 'small';

  @Input() readonly: boolean = false;
    @Input() edit: boolean = true; // edit
    @Input() schoolObject!: any;
    // hasLogged : boolean = false;
    id!: number;
    status: any = null;
    role: any = null;
    ownerId: number = 0; // FIXME
    provinces: any[] = [];
    selectedProvince: any = '';
    selectedDistrict: any = '';
    selectedWard: any = '';
    districtOptions: any[] = [];
    wardOptions: any[] = [];
    isSubmittingForm = false;
    listSchoolType: string[] = [];
    admissionAge: string[] = [];
    eductionMethod: string[] = [];
    facilities: any[] = [];
    utilities: any [] = [];
    text: string = '';
    fileList: any[] = [];
    mainFileList: any[] = [];
    hoveredImg: any;
    selectedCityId!: number;
    selectedCityName!: string;

  oldMainImage!: any;
  oldMainImageId!: any;

  oldOtherImages: any[] = [];
  oldImagesId: any[] = [];
  imageStates: { [key: number]: { deleted: boolean } } = {};
  copyOldImagesId!: any[];

  undoMode: boolean = false;
  deleteImgSrc: string = 'https://i.pinimg.com/originals/2f/f0/8c/2ff08c693ddbe8f07c1509a26f7006a9.png';
  confirmModal?: NzModalRef;
  schoolUtilities: any = [];
  schoolFacilities: any = [];
  validateForm!: UntypedFormGroup;

  // for school owner list
  optionList: any[] = [];

  constructor(private fb: FormBuilder,
              private _schoolService: SchoolService,
              private _router: Router,
              private _activatedRouter: ActivatedRoute,
              private notification: NzNotificationService,
              private _adminSchoolService: AdminSchoolService,
              private _userService: UserService,
              private _emailService: MailService,
              private modal: NzModalService,
              private _authService: AuthService
  ) {
  }

  log(value: string[]): void {
    this.schoolUtilities = value;
  }

  log2(value: string[]): void {
    this.schoolFacilities = value;
  }

  // formatterNumber = (value: number): string => {
  //
  //   // Use toLocaleString with the 'en-US' locale to format the number with commas
  //   return value.toLocaleString('vi-VN');
  // };
  //   parseFeeValue(value: string): any {
  //       // Remove commas from the formatted value and parse it to a number
  //       value.replace('.', '');
  //   }
  //   // parseFeeValue = (value: string): string => value.trim().replace('.', '');

  // loading the school owner
  loadAllOwnerSchool() {
    this._userService.getAllOwnerSchool().subscribe({
      next: response => {
        this.optionList = response;
      },
      error: error => {
        console.error('Error:', error);
      }
    })
  }


  ngOnInit(): void {
    this.loadFacility();
    this.loadUtility();
    if (this.schoolObject) {
      this.status = this.schoolObject!.status
      this.selectedCityId = this.schoolObject?.city?.id;
      this.selectedCityName = this.schoolObject?.city?.name;
      this.oldMainImage = this.schoolObject?.mainImage;
      this.oldOtherImages = this.schoolObject?.images;
      this.oldMainImageId = this.schoolObject?.mainImage?.id;
      for (let img of this.schoolObject?.images) {
        this.oldImagesId.push(img.id);
      }
    }

    // @ts-ignore
    const ROLES = this._authService.getRolesFromAccessToken().roles;
    this.role = ROLES.substring(5);

        this.copyOldImagesId = [...this.oldImagesId];
        this.validateForm = this.fb.group({
            id: [this.schoolObject?.id || this.id || null],
            oldMainImageId: [this.oldMainImageId || null],
            oldImagesId: this.fb.array(this.copyOldImagesId.map((id: any) => this.fb.control(id))),
            ownerId: [this.schoolObject?.ownerId || ''],
            name: [this.schoolObject?.name || '', [Validators.required]],
            schoolAddress: ['', [Validators.required]],
            type: [this.schoolObject?.type || '', [Validators.required]],
            website: [this.schoolObject?.website || ''],
            cityId: [this.schoolObject?.city?.id || '', [Validators.required]] || this.selectedProvince,
            districtId: [this.schoolObject?.district?.id || '', [Validators.required]] || this.selectedDistrict,
            wardId: [this.schoolObject?.ward?.id || '', [Validators.required]] || this.selectedWard,
            street: [this.schoolObject?.street || '', [Validators.required]],
            educationMethod: [this.schoolObject?.educationMethod || '', [Validators.required]],
            email: [this.schoolObject?.email || '', [Validators.required, Validators.pattern("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")]],
            phoneNumber: [this.schoolObject?.phoneNumber || '', [Validators.required, Validators.pattern("^(0[35789][0-9]{8}|02[0-9]{9})$")]],
            childReceivingAge: [this.schoolObject?.childReceivingAge || '', [Validators.required]],
            minFee: [this.schoolObject?.minFee || null, [Validators.required]], // Initial value for 'minFee' control (null if not known)
            maxFee: [this.schoolObject?.maxFee || null, [Validators.required]], // Initial value for 'maxFee' control (null if not known)
            schoolFacilitiesId: this.fb.array([]).setValue(this.schoolFacilities),
            schoolUtilitiesId: this.fb.array([]).setValue(this.schoolUtilities),
            introduction: [this.schoolObject?.introduction || '', [Validators.required, Validators.minLength(0), Validators.maxLength(4000)]] // Initial value for 'introduction' control
        });
        this.loadProvince();
        this.loadSchoolType();
        this.loadAgeOptions();
        this.loadEducationMethod();
        this.detechWardChange();
        this.detechChange();
        if (this.schoolObject?.city && this.schoolObject?.district) {
            this._schoolService.getAllDistrictByProvinceID(this.schoolObject?.city?.id).subscribe(district => {
                this.districtOptions = district;
                this.validateForm.get('districtId')?.setValue(this.schoolObject?.district?.id);
            });
            this._schoolService.getAllWardByDistrictID(this.schoolObject?.district?.id).subscribe(ward => {
                this.wardOptions = ward;
                this.validateForm.get('wardId')?.setValue(this.schoolObject?.ward?.id);
            })
        }
        // @ts-ignore
        this._activatedRouter.paramMap.subscribe(params => this.id = +params.get('id'))
    }

  detechChange() {
    this.validateForm.get('cityId')?.valueChanges.subscribe(selectedCity => {
      this.districtOptions = [];
      this.validateForm.get('districtId')?.setValue('');
      this.changeDistrict(selectedCity);
    })
  }

  changeDistrict(selectedCity: any) {
    if (!selectedCity) {
      return;
    }
    this._schoolService.getAllDistrictByProvinceID(selectedCity).subscribe(district => {
      this.districtOptions = district;
      this.validateForm.get('districtId')?.setValue('');
    })
  }

  detechWardChange() {
    this.validateForm.get('districtId')?.valueChanges.subscribe(selectedDistrict => {
      this.wardOptions = [];
      this.validateForm.get('wardId')?.setValue('');
      this.changeWard(selectedDistrict);
    })
  }

  changeWard(selectedDistrict: any) {
    if (selectedDistrict) {
      if (selectedDistrict) {
        this._schoolService.getAllWardByDistrictID(selectedDistrict).subscribe(ward => {
          this.wardOptions = ward;
        })
      }
    }
  }

  validateFormData() {
    this.validateForm.get('id')?.setValue(this.id || null);
    this.validateForm.get('schoolFacilitiesId')?.setValue(this.schoolFacilities);
    this.validateForm.get('schoolUtilitiesId')?.setValue(this.schoolUtilities);
    this.validateForm.get('schoolAddress')?.setValue(['ok']);

    this.filterElementsByIndex(this.copyOldImagesId, this.imageStates);
    this.validateForm.get('oldImagesId')?.patchValue(this.copyOldImagesId || []);

    this.validateForm.get('oldMainImageId')?.setValue(this.oldMainImageId);
    if (this.validateForm.invalid) {
      // const minFeeValue = this.parseFeeValue(this.validateForm.get('minFee')?.value)
      // console.log(minFeeValue)
      console.log(this.validateForm.value)
      const schoolFormPayload = this.validateForm.value;
      delete schoolFormPayload.schoolAddress;
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({onlySelf: true});
        }
      });
      return false;
    }
    return true;
  }

  preparedDataRequest() {
    const formData: FormData = new FormData();
    if (this.fileList) {
      for (let file of this.fileList) {
        formData.append('images', file);
      }
    }
    if (this.mainFileList && this.mainFileList[0]) {
      formData.append('mainImage', this.mainFileList[0]);
    }
    const schoolFormPayload = this.validateForm.value;
    delete schoolFormPayload.schoolAddress;
    formData.append('school', new Blob([JSON.stringify(schoolFormPayload)], {
      type: 'application/json'
    }));
    return formData;
  }

  // hiển thị modal
  showApprovedConfirm(event: string): void {
    if (this.readonly) {
      this.confirmModal = this.modal.confirm({
        nzTitle: this.capitalizeFirstLetter(event) + ' school?',
        nzContent: 'Are you sure you want to ' + event + ' this school',
        nzCancelText: 'No, take me back!',
        nzOnOk: () =>
          this.patchForm(event)
      });
    } else if (!this.readonly && this.edit && this.status) {
      this.confirmModal = this.modal.confirm({
        nzTitle: this.capitalizeFirstLetter(event) + ' school?',
        nzContent: 'Are you sure you want to ' + event + ' this school',
        nzCancelText: 'No, take me back!',
        nzOnOk: () =>
          this.putForm(event)
      });
    }
  }

    showDeleteConfirm(event: string): void {
        this.modal.confirm({
            nzTitle: 'Delete school',
            nzContent: '<b >Are you sure delete this school?</b> </br>' +
                '<p style="color: red"> This action is permanent and cannot be undone!</>',
            nzOkText: 'Delete',
            nzOkType: 'primary',
            nzOkDanger: true,
            nzOnOk: () => this.deleteForm(event),
            nzCancelText: 'No, please take me back',
            nzOnCancel: () => console.log('Cancel')
        });
    }

  submitForm(event: string) {
    if (this.readonly) {
      this.patchForm(event)
    } else if (!this.readonly && this.edit && this.status) {
      this.putForm(event)
    } else if (!this.readonly && !this.edit && !this.status) {
      this.postForm(event)
    }
  }

  putForm(event: string) {
    if (this.validateFormData()) {
      this.isSubmittingForm = true;
      const formData: FormData = this.preparedDataRequest();
      this._adminSchoolService.putSchool(formData, event)
        .pipe(finalize(() => this.isSubmittingForm = false))
        .subscribe({
          next: res => {
            this.notification.success('Congratulation!', 'Submission successful')
            if (event == "submit" || event == 'reject' || event == 'approve') {
              this.sendMail(event, this.id)
            }
            this._router.navigateByUrl('/admin/school');
          },
          error: err => {
            if (err.status == 400) {
              this.notification.error('', err.error);
            } else {
              this.notification.error('Sorry!', ` Request failed. Please try again later.`);
            }
          }
        });
    }
  }

  postForm(event: string): void {
    if (this.validateFormData()) {
      this.isSubmittingForm = true;
      const formData: FormData = this.preparedDataRequest();
      this._adminSchoolService.postSchool(formData, event)
        .pipe(finalize(() => this.isSubmittingForm = false))
        .subscribe({
          next: res => {
            this.notification.success('Congratulation!', 'Saved school successfully.')
            if (event == "submit") {
              this.sendMail(event, res.id)
            }
            this._router.navigateByUrl('/admin/school');
          },
          error: err => {
            if (err.status == 400) {
              this.notification.error('', err.error);
            } else {
              this.notification.error('Sorry!', ` Create/Update failed. Please try again later.`);
            }
          }
        });
    }
  }

  patchForm(event: string) {
    this._adminSchoolService.patchSchool(event, this.id)
      .pipe(finalize(() => this.isSubmittingForm = false))
      .subscribe({
        next: res => {
          this.notification.success('Success!', `${this.capitalizeFirstLetter(event)} successful`)

          if (event == "submit" || event == 'approve' || event == 'reject') {
            console.log('co chạy vào đây không ')
            this.sendMail(event, this.id)
          }

          this._router.navigateByUrl('/admin/school');
        },
        error: err => {
          if (err.status == 400) {
            this.notification.error('', err.error);
          } else {
            this.notification.error('Sorry!', ` Create/Update failed. Please try again later.`);
          }
        }
      });
  }

  deleteForm(event: string) {
    this.id = this.schoolObject!.id
    this._adminSchoolService.deleteSchool(this.id)
      .pipe(finalize(() => this.isSubmittingForm = false))
      .subscribe({
        next: res => {
          this.notification.success('', 'You have deleted a school')
          this._router.navigateByUrl('/admin/school');
        },
        error: err => {
          if (err.status == 400) {
            this.notification.error('', err.error);
          } else {
            this.notification.error('Sorry!', ` failure to delete the school. Please try again later.`);
          }
        }
      });
  }

  sendMail(status: string, schoolId: number) {
    switch (status) {
      case 'submit':
        this._emailService.schoolSubmit(status, schoolId).subscribe({
          next: (response: any) => {
            this.notification.success('', 'Your Submission Request has been sent to The Admin')
          }, error: (error: any) => {
            this.notification.error('', 'failure to sending your request to admin, please try it later')
          }
        });
        break;
      case 'approve':
        this._emailService.schoolSubmit(status, schoolId).subscribe({
          next: (response: any) => {
            this.notification.success('', 'There was an email will sent to the school owner')
          }, error: (error: any) => {
            this.notification.error('', 'failure to send an approve email to the school owner, please try it later')
          }
        });
        break;
      case 'reject':
        this._emailService.schoolSubmit(status, schoolId).subscribe({
          next: (response: any) => {
            this.notification.success('', 'There was an email will sent to the school owner')
          }, error: (error: any) => {
            this.notification.error('', 'failure to send an reject email to the school owner, please try it later')
          }
        });
        break;
      default :
        return;
    }
  }

  capitalizeFirstLetter(inputString: string): string {
    if (inputString.length === 0) {
      return inputString; // Xử lý trường hợp chuỗi rỗng (empty string)
    }

    const firstLetter = inputString.charAt(0).toUpperCase();
    const restOfString = inputString.slice(1); // Lấy phần còn lại của chuỗi

    return firstLetter + restOfString;
  }

  resetForm(): void {
    this.validateForm.disabled;
  }

  loadFacility(): void {
    this._schoolService.getFacility().subscribe({
      next: (response: any[]) => {
        if (response) {
          this.facilities = response.map((item: any) => ({
            id: item.id,
            name: item.name,
            code: item.code,
          }));
        } else {
          console.error("Invalid response format:");
        }
      },
      error: (error: any) => {
        console.error('Error loading Facility options:', error);
      }
    });
  }

  loadProvince() {
    this._schoolService.getAllProvince().subscribe({
      next: resp => {
        this.provinces = resp;
      },
      error: err => {
        this.provinces = [];
      }
    });
  }

  loadSchoolType() {
    this._schoolService.getSchoolTypes().subscribe(listType => {
      this.listSchoolType = listType
    })
  }

  loadAgeOptions(): void {
    this._schoolService.getAdmissionAge().subscribe({
      next: response => {
        this.admissionAge = response;
      },
      error: error => {
        console.error('Error loading age options:', error);
      }
    });
  }

  loadEducationMethod(): void {
    this._schoolService.getEducationMethod().subscribe({
      next: response => {
        this.eductionMethod = response;
      },
      error: error => {
        console.error('Error loading age options:', error);
      }
    });
  }

  loadUtility(): void {
    this._schoolService.getUtility().subscribe({
      next: (response: any[]) => {
        if (response) {
          this.utilities = response.map((item: any) => ({
            id: item.id,
            name: item.name,
            code: item.code,
          }));
        } else {
          console.error("Invalid response format");
        }
      },
      error: (error: any) => {
        console.error('Error loading Facility options:');
      }
    });
  }

  displayImg(img: string) {
    if (img.startsWith('http')) {
      return img;
    }
    img = img.replace("\\", "/")
    return `http://localhost:8686/public/files/${img}`;
  }

  handleOtherImageChange(event: any) {
    const inputElement = event.target as HTMLInputElement;
    const files = inputElement.files;

    if (files && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        this.fileList.push(files[i]);
      }
    }
  }

  handleMainImageChange(event: any) {
    const inputElement = event.target as HTMLInputElement;
    const files = inputElement.files;

    if (files && files.length > 0) {
      this.mainFileList[0] = files[0];
    }
    console.log(this.mainFileList)
  }

  deleteMainImage() {
    if (this.mainFileList.length > 0) {
      this.mainFileList.shift();
    }
  }

  deleteOtherImage(name: any) {
    const index = this.fileList.findIndex(file => file.name === name);
    if (index !== -1) {
      this.fileList.splice(index, 1);
    }
  }

  deleteOldMainImage() {
    this.oldMainImage = null;
    this.oldMainImageId = null;
    this.undoMode = true;
  }

  undoOldMainImage() {
    this.oldMainImage = this.schoolObject.mainImage;
    this.oldMainImageId = this.schoolObject?.mainImage?.id;
    this.undoMode = false;
  }

  deleteOldOtherImages(index: number) {
    this.imageStates[index] = {deleted: true};
    this.copyOldImagesId = [...this.oldImagesId];
  }

  undoOldOtherImages(index: number) {
    if (this.imageStates[index]) {
      this.imageStates[index].deleted = false;
      this.copyOldImagesId = [...this.oldImagesId];
    }
  }

  filterElementsByIndex(array: number[], indexObject: { [key: string]: { deleted: boolean } }) {
    for (const key in indexObject) {
      if (indexObject.hasOwnProperty(key) && indexObject[key].deleted) {
        const indexToDelete = parseInt(key);
        if (!isNaN(indexToDelete) && indexToDelete >= 0 && indexToDelete < array.length) {
          array[indexToDelete] = -1;
        }
      }
    }
  }

  ngAfterViewInit(): void {
    this.schoolFacilities = this.schoolObject?.schoolFacilities.map((facility: {
      facility: { id: any; };
    }) => facility.facility.id);
    this.schoolUtilities = this.schoolObject?.schoolUtilities.map((utility: {
      utility: { id: any; };
    }) => utility.utility.id);
  }

  protected readonly onsubmit = onsubmit;

}
