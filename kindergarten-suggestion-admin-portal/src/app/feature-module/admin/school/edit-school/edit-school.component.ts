import { Component } from '@angular/core';
import {SchoolService} from "../../../../service/school-service/school.service";
import {ActivatedRoute} from "@angular/router";
import {UntypedFormBuilder} from "@angular/forms";
import {AdminSchoolService} from "../../../../service/admin-school-service/admin-school.service";

@Component({
  selector: 'app-edit-school',
  templateUrl: './edit-school.component.html',
  styleUrls: ['./edit-school.component.scss']
})
export class EditSchoolComponent {
  schoolDetail! : any;
  id : number = 0;
  constructor(private _adminSchoolService : AdminSchoolService,
              private _activatedRoute : ActivatedRoute,
              private _formBuilder : UntypedFormBuilder) {
  }

  ngOnInit(): void {
    // @ts-ignore
    this._activatedRoute.paramMap.subscribe( params => this.id = +params.get('id'))
    this.loadSchoolDetail();
  }

  loadSchoolDetail() {
    this._adminSchoolService.getSchoolDetail(this.id).subscribe({
      next : resp => {
        console.log(resp); // FIXME
        this.schoolDetail = {
          id : resp.id,
          ownerId: resp.ownerId,
          name: resp.name,
          phoneNumber : resp.phoneNumber,
          email : resp.email,
          city : resp.city,
          district : resp.district,
          ward : resp.ward,
          type : resp.type,
          website : resp.website,
          street : resp.street,
          educationMethod : resp.educationMethod,
          childReceivingAge : resp.childReceivingAge,
          minFee : resp.minFee,
          maxFee : resp.maxFee,
          introduction : resp.introduction,
          schoolFacilities : resp.schoolFacilities,
          schoolUtilities : resp.schoolUtilities,
          images : resp.images,
          mainImage : resp.mainImage,
          status: resp.status
        };
      }, error : err => {
        console.error(err)
      }
    })
  }

}
