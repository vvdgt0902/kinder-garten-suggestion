import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SchoolRoutingModule } from './school-routing.module';

import { NzFormModule } from "ng-zorro-antd/form";
import { NzBreadCrumbModule } from "ng-zorro-antd/breadcrumb";
import { ViewCrudSchoolComponent } from "./view-crud-school/view-crud-school.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NzSelectModule } from "ng-zorro-antd/select";
import { NzCheckboxModule } from "ng-zorro-antd/checkbox";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzInputNumberModule } from "ng-zorro-antd/input-number";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { NzUploadModule } from "ng-zorro-antd/upload";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzMessageService } from "ng-zorro-antd/message";
import { SchoolTypePipe } from "../../../pipe/school-type.pipe";
import { ChildReceivingAgeDisplayNamePipe } from "../../../pipe/child-receiving-age-display-name.pipe";
import { EducationDisplayPipe } from "../../../pipe/education-display.pipe";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { SchoolDetailComponent } from './school-detail/school-detail.component';
import { SchoolListComponent } from './school-list/school-list.component';
import { AddSchoolComponent } from "./add-school/add-school.component";
import { RatingStarComponent } from './ratings/rating-star/rating-star.component';
import { NzRateModule } from "ng-zorro-antd/rate";
import { RatingsFeedbacksComponent } from './ratings/ratings-feedbacks/ratings-feedbacks.component';
import { NzDatePickerModule } from "ng-zorro-antd/date-picker";
import { NzTabsModule } from "ng-zorro-antd/tabs";
import { NzCardModule } from "ng-zorro-antd/card";
import { NzSwitchModule } from "ng-zorro-antd/switch";
import { NzAvatarModule } from "ng-zorro-antd/avatar";
import { NzSkeletonModule } from "ng-zorro-antd/skeleton";
import { NzListModule } from "ng-zorro-antd/list";
import { CamelCaseToSpacePipe } from "../../../pipe/camel-case-to-space.pipe";
import { NzBackTopModule } from "ng-zorro-antd/back-top";
import { NzImageModule } from "ng-zorro-antd/image";
import { NzTagModule } from "ng-zorro-antd/tag";
import { EditSchoolComponent } from './edit-school/edit-school.component';
import { NzModalModule } from "ng-zorro-antd/modal";
import { NzEmptyModule } from "ng-zorro-antd/empty";
import { DobDatePipe } from "../../../pipe/dob-date.pipe";
import { NzPopconfirmModule } from "ng-zorro-antd/popconfirm";
import { NzTableModule } from "ng-zorro-antd/table";
import { NzSpaceModule } from "ng-zorro-antd/space";

import { NzPaginationModule } from "ng-zorro-antd/pagination";


@NgModule({
  providers: [
    NzMessageService,
    NzNotificationService
    // other providers
  ],
  declarations: [
    ViewCrudSchoolComponent,
    SchoolDetailComponent,
    SchoolListComponent,
    AddSchoolComponent,
    RatingStarComponent,
    RatingsFeedbacksComponent,
    CamelCaseToSpacePipe,
    EditSchoolComponent
  ],
  exports: [
    ViewCrudSchoolComponent,
    RatingStarComponent
  ],
  imports: [
    CommonModule,
    SchoolRoutingModule,
    NzFormModule,
    NzBreadCrumbModule,
    ReactiveFormsModule,
    NzSelectModule,
    NzCheckboxModule,
    NzInputModule,
    FormsModule,
    NzButtonModule,
    NzInputNumberModule,
    CKEditorModule,
    NzUploadModule,
    NzIconModule,
    SchoolTypePipe,
    ChildReceivingAgeDisplayNamePipe,
    EducationDisplayPipe,
    NzRateModule,
    NzDatePickerModule,
    NzCardModule,
    NzSwitchModule,
    NzAvatarModule,
    NzSkeletonModule,
    NzListModule,
    NzBackTopModule,
    NzModalModule,
    NzImageModule,
    NzTagModule,
    NzEmptyModule,
    DobDatePipe,
    NzPopconfirmModule,
    NzTableModule,
    NzSpaceModule,
    NzTabsModule,
    NzPaginationModule
  ]
})
export class SchoolModule {
}
