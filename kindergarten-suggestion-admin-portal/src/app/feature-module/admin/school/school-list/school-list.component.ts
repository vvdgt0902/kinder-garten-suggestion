import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import {Subject, takeUntil} from "rxjs";
import {AdminSchoolService} from "../../../../service/admin-school-service/admin-school.service";
import {AuthService} from "../../../../service/auth-service/auth.service";
import {NzNotificationService} from "ng-zorro-antd/notification";

@Component({
  selector: 'app-school-list',
  templateUrl: './school-list.component.html',
  styleUrls: ['./school-list.component.scss']
})
export class SchoolListComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();
  schoolList: any[] = [];
  searchOptions: string = '';
  dataLoaded = false;
  role: string = '';
  page: number = 0;
  size: number = 5;
  totalElements: number = 0;
  constructor(private _adminSchoolService: AdminSchoolService,
              private _activatedRoute : ActivatedRoute,
              private _router: Router,
              private _authService: AuthService,
              private _notification : NzNotificationService
  ) {
  }

  ngOnInit(): void {
    // @ts-ignore
    const ROLES = this._authService.getRolesFromAccessToken().roles;
    this.role = ROLES.substring(5)

    this._activatedRoute.queryParams.subscribe(params => {
      this.page = Number(params['page']) || 0;
      this.size = Number(params['size']) || 10;
      this.searchOptions = params['query'] || '';
    });
    this.loadData();
  }

  search() {
    this.reGenerateUrl();
    this.loadData();
  }

  loadData() {
    this._adminSchoolService.getSchoolList(this.searchOptions, this.page, this.size).pipe(takeUntil(this.unsubscribe$)).subscribe({
      next: res => {
        this.schoolList = res.content;
        console.log(res)
        this.totalElements = res.totalElements;
        this.dataLoaded = true;
      },
      error: error => {

      }
    });
  }
  reGenerateUrl() {
    const navigationExtras: NavigationExtras = {
      queryParams: {query: this.searchOptions, page: this.page, size: this.size}
    };
    this._router.navigate([], navigationExtras);
  }

  paging(newPage: number) {
    this.page = newPage - 1;
    this.reGenerateUrl();
    this.loadData();
  }

  delete(id: number): void {
    this._adminSchoolService.deleteSchool(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: () => {
          this.createNotification('success', 'Success deleted!', '');
          this.loadData();
        },
        error: error => {
        }
      });
  }

  createNotification(type: string, title: string, message: string): void {
    this._notification.create(
      type,
      title, message
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
