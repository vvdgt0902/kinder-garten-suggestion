import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from "@angular/router";
import { Subject, takeUntil } from "rxjs";
import { PreviousPageService } from 'src/app/service/previous-page/previous-page.service';
import { RequestService } from 'src/app/service/request-service/request.service';

@Component({
  selector: 'app-remainder-request',
  templateUrl: './remainder-request.component.html',
  styleUrls: ['./remainder-request.component.scss']
})
export class RemainderRequestComponent implements OnInit {
  private unsubscribe$: Subject<void> = new Subject<void>();
  reminderList: any[] = [];
  keywordOpt: string = '';
  page = 0;
  size = 5;
  totalElements = 0;
  dataLoaded = false;

  constructor(private _requestService: RequestService,
    private _router: Router,
    private _activeRouter: ActivatedRoute,
    private _previous: PreviousPageService) {
  }

  ngOnInit(): void {

    const role = localStorage.getItem('role');
    if (!role || (!role.includes("ADMIN") && !role.includes("SCHOOL_OWNER"))) {
      this._previous.setPreviousUrl(window.location.pathname);
      this._router.navigateByUrl('/admin/auth/login');
    }
    this._activeRouter.queryParams.subscribe(params => {
      this.page = Number(params['page']) || 0;
      this.size = Number(params['size']) || 5;
      this.keywordOpt = params['q'];
    });


    this.loadData();
  }

  loadData() {
    this._requestService.getReminderRequest(this.page, this.size, this.keywordOpt).pipe(takeUntil(this.unsubscribe$)).subscribe(res => {
      this.reminderList = res.content;
      console.log(this.reminderList)
      this.totalElements = res.totalElements;
      this.dataLoaded = true;
    }, error1 => {
      console.log("err")
    }
    )
  }
  search() {
    this.loadData();
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  paging(newPage: number) {
    this.page = newPage - 1;
    this.reGenerateUrl();
    this.loadData();
  }
  reGenerateUrl() {
    const navigationExtras: NavigationExtras = {
      queryParams: { query: this.keywordOpt, page: this.page, size: this.size }
    };
    this._router.navigate([], navigationExtras);
  }
  capitalizeFirstLetter(inputString: string): string {
    if (inputString.length === 0) {
      return inputString; // Xử lý trường hợp chuỗi rỗng (empty string)
    }
    const firstLetter = inputString.charAt(0).toUpperCase();
    const restOfString = inputString.slice(1).toLowerCase(); // Lấy phần còn lại của chuỗi

    return firstLetter + restOfString;
  }
}
