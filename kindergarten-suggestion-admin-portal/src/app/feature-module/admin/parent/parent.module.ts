import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NzBreadCrumbModule } from "ng-zorro-antd/breadcrumb";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzCardModule } from "ng-zorro-antd/card";

import {NzTagModule} from "ng-zorro-antd/tag";
import {RequestStatusPipe} from "../../../pipe/request-status.pipe";
import { SchoolModule } from '../school/school.module';
import { ParentListComponent } from './parent-list/parent-list.component';
import { ParentRoutingModule } from './parent-routing.module';
import { SchoolListEnrollComponent } from './school-list-enroll/school-list-enroll.component';
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzSelectModule} from "ng-zorro-antd/select";
import {DobDatePipe} from "../../../pipe/dob-date.pipe";
import {ParentDetailComponent} from "./parent-detail/parent-detail/parent-detail.component";

@NgModule({
  declarations: [
    ParentListComponent,
    ParentDetailComponent,
    SchoolListEnrollComponent
  ],
    imports: [
        CommonModule,
        ParentRoutingModule,
        FormsModule,
        NzBreadCrumbModule,
        NzButtonModule,
        NzTableModule,
        NzInputModule,
        NzIconModule,
        NzPaginationModule,
        DobDatePipe,
        NzCardModule,
        NzSelectModule,
        ReactiveFormsModule,
        SchoolModule,
        NzModalModule,
        NzPopconfirmModule,
        NzTagModule,
        RequestStatusPipe
    ]
})
export class ParentModule { }
