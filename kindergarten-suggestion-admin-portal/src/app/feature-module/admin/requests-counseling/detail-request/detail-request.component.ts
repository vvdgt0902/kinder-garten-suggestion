import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from '@angular/router';
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {RequestService} from 'src/app/service/request-service/request.service';
import {NzNotificationComponent, NzNotificationService} from "ng-zorro-antd/notification";

@Component({
  selector: 'app-detail-request',
  templateUrl: './detail-request.component.html',
  styleUrls: ['./detail-request.component.scss']
})

export class DetailRequestComponent implements OnInit {
  readonly: boolean = true;
  requestForm!: FormGroup;
  confirmModal?: NzModalRef;
  requestId!: number;
  requestDetail: any = {};
  loadingError: boolean = false;
  address = " ";

  constructor(private fb: FormBuilder,
              private modal: NzModalService,
              private requestService: RequestService,
              private _activeRoute: ActivatedRoute,
              private _router: Router,
              private _nzNotificationService: NzNotificationService) {
  }

  ngOnInit(): void {
    this._activeRoute.paramMap.subscribe(params => {
      const idString = params.get('id');
      if (idString) {
        this.requestId = parseInt(idString);
        console.log(this.requestId)
      }
    })
    this.requestForm = this.fb.group({
        fullName: [null, [Validators.required]],
        email: [null, [Validators.required, Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
        phoneNumber: [null, [Validators.required, Validators.pattern(/^(0[35789][0-9]{8}|02[0-9]{9})$/)]],
        schoolAddress: [null],
        requestSchool: [null],
        inquiry: [null]

      }
    )
    this.loadData(this.requestId);
  }

  loadData(id: number) {
    this.requestService.getRequestDetailById(id).subscribe({
      next: response => {
        this.requestDetail = response;
        console.log(this.requestDetail);
        if (this.requestDetail) {
          this.requestForm.setValue({
            fullName: this.requestDetail.fullName,
            email: this.requestDetail.email,
            phoneNumber: this.requestDetail.phoneNumber,
            schoolAddress: ((this.requestDetail?.user?.street ? this.requestDetail.user.street + ", " : "")
              + (this.requestDetail?.user?.ward ? this.requestDetail.user.ward.name + ", " : "")
              + (this.requestDetail?.user?.district ? this.requestDetail.user.district.name + ", " : "")
              + (this.requestDetail?.user?.city ? this.requestDetail.user.city.name : "")),
            requestSchool: this.requestDetail.school.name,
            inquiry: this.requestDetail.inquiry
          });
        }
      }, error: error => {
        this.loadingError = true; // Đánh dấu có lỗi xảy ra
        // Sử dụng setTimeout để hiển thị thông báo sau 1 giây
        setTimeout(() => {
          alert("Error when loading User Data");
          //Chuyển hớng về /user
          this._router.navigate(['/admin/request']);
        }, 500);
      }
    })
  }

  enroll() {
    this.confirmModal = this.modal.confirm({
      nzTitle: 'To the enroll page?',
      nzContent: 'Would you like to mark it as resolved and proceed to the enroll page?',
      nzCancelText: 'No, take me back!',
      nzOnOk: () => {
        this.requestService.updateStatusRequest(this.requestId).subscribe({
          next: res => {

            this._router.navigate(['/admin/parent/detail', this.requestDetail?.user?.id]).then(() =>
              this.showNoti('success', '', 'Mark request resolve success!'));
            localStorage.setItem("schoolId", this.requestDetail?.school?.id);
          },
          error: err => {
            // Xử lý lỗi nếu cần
            this.showNoti('error', "Update Failed", '');
          }
        });
      }
    });
  }

  showConfirm(): void {
    this.confirmModal = this.modal.confirm({
      nzTitle: 'Mark request resolve?',
      nzContent: 'Are you sure you want to mark request as resolve',
      nzCancelText: 'No, take me back!',
      nzOnOk: () => {
        this.requestService.updateStatusRequest(this.requestId).subscribe({
          next: res => {
            //cập nhật giao diện
            this.showNoti('success', '', 'Mark request resolve success!');
            this.loadData(this.requestId);
          },
          error: err => {
            // Xử lý lỗi nếu cần
            this.showNoti('error', "Update Failed", '');
          }
        });
      }
    });
  }

  showNoti(type: string, title: string, message: string) {
    this._nzNotificationService.create(
      type,
      title, message
    );
  }
}
