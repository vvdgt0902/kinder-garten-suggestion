import { Component, Input, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NzNotificationService} from "ng-zorro-antd/notification";
import {UserService} from "../../../../service/user-service/user.service";

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  userDetail: any =  {}; // Dữ liệu người dùng sẽ được lấy từ API
  userId: number = 0;
  loadingError: boolean = false;

  constructor(
    private _activeRoute: ActivatedRoute,
    private _userService: UserService,
    private _router: Router,
    private notification: NzNotificationService
  ) { }

  ngOnInit(): void {
    this._activeRoute.paramMap.subscribe(params => {
      const idString = params.get('id');
      if(idString){
        this.userId = parseInt(idString);
        console.log(this.userId)
      }
    })
    this.loadData();
  }

  deactiveNotification(type: string): void {
    this.notification.create(
      type,
      'Update success!!',
      'Account has been deactivated'
    );
  }
  activeNotification(type: string): void {
    this.notification.create(
      type,
      'Update success!!',
      'Account has been activated'
    );
  }

  loadData(){
    this._userService.getUserDetailById(this.userId).subscribe({
      next: response =>{
        this.userDetail = response;
        console.log(this.userDetail)
      },error:error=>{
        console.error("Lỗi khi tải dữ liệu người dùng: ", error);
        this.loadingError = true; // Đánh dấu có lỗi xảy ra
        // Sử dụng setTimeout để hiển thị thông báo sau 3 giây
        setTimeout(() => {
          alert("Error when loading User Data");
          //Chuyển hớng về /user
          this._router.navigate(['/user']);
        }, 3000);
      }
    });
  }
  updateUserStatus(id: number) {
    this._userService.updateUserStatus(this.userId).subscribe({
      next: () => {
        // Cập nhật giao diện người dùng
        this.loadData();
      },
      error: () => {
        // Xử lý lỗi nếu cần
        this.showErrorMessage("Update Failed");
      }
    });
  }
  showErrorMessage(message: string) {
    alert("Update failed")
  }
}
