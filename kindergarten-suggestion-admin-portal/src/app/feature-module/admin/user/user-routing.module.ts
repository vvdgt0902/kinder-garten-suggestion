import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ViewListComponent} from "./view-list/view-list.component";
import {AddUserComponent} from "./add-user/add-user.component";
import {EditUserComponent} from "./edit-user/edit-user.component";
import {UserDetailComponent} from "./user-detail/user-detail.component";

const routes: Routes = [
  { path: '', component: ViewListComponent },
  { path: 'list', component: ViewListComponent },
  { path: 'add', component: AddUserComponent },
  { path: 'edit/:id', component: EditUserComponent },
  { path: 'detail/:id', component: UserDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
