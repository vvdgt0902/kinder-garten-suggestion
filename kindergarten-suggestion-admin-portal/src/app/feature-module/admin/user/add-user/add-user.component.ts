import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AdminService } from 'src/app/service/admin-service/admin.service';
import { MailService } from 'src/app/service/mail-service/mail.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  createForm!: FormGroup;
  successMessage: string = "Successfully added!";
  failMessage: string = "Failed!";
  message!: string;
  accessToken: any = JSON.stringify(localStorage.getItem('accessToken'));
  roles: any[] = [];

  constructor(private _formBuilder: FormBuilder,
    private _adminService: AdminService,
    private notification: NzNotificationService,
    private _router: Router,
    private _mailService: MailService) {
  }


  ngOnInit(): void {
    console.log("😜😜😜 ~ file: add-user.component.ts:28 ~ AddUserComponent ~ ngOnInit ~ this.accessToken:", this.accessToken)
    if (this.accessToken === 'null') {
      console.log('failure');
      this._router.navigateByUrl('admin/auth/login');
    } else {
      console.log('failure2');
      this.createForm = this._formBuilder.group({
        fullName: [null, [Validators.required]],
        email: [null, [Validators.required, Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
        dob: [null, [Validators.required, this.dateValidator]],
        phoneNumber: [null, [Validators.required, Validators.pattern(/^(0[35789][0-9]{8}|02[0-9]{9})$/)]],
        role: [null, [Validators.required]],
        active: [null],
      });
      this.getRole();
    }
  }

  submitForm(): void {
    if (this.createForm.valid) {
      console.log('submit', this.createForm.value);
      console.log(typeof this.createForm.controls['active'].value);
      this._adminService.createUserByAdmin(this.createForm.value).subscribe({
        next: res => {
          this.message = 'Create successfully!'
          this.sendMail();
          this.createForm.reset();
          this._router.navigateByUrl('/admin/user/list');
          this.createNotification('success', this.successMessage, this.message);
        },
        error: err => {
          this.message = err.error;
          this.createNotification('error', this.failMessage, this.message);
        }
      })

    } else {
      Object.values(this.createForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  dateValidator = (control: UntypedFormControl): { [s: string]: boolean } => {

    if (!control.value) {
      return { required: true };
    }
    const currentDate = Date.now();
    const date = new Date(control.value);
    if (date.getTime() >= currentDate) {
      return { confirm: true, error: true };
    }
    return {};
  }

  createNotification(type: string, title: string, message: string): void {
    this.notification.create(
      type,
      title, message
    );
  }

  sendMail() {
    console.log(this.createForm.value)
    const payload = this.createForm.value;
    this._mailService.createNewUser(payload).subscribe({
      next: res => {
      },
      error: err => {
      }
    })
  }

  getRole() {
    this._adminService.getRoleUser().subscribe({
      next: res => {
        console.log(res);
        this.roles = res;
      },
      error: err => {
        console.log(err);
      }
    })
  }
}


