import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RouterLink, RouterOutlet } from "@angular/router";
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzDropDownModule } from "ng-zorro-antd/dropdown";
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from "ng-zorro-antd/menu";
import { FooterComponent } from './private-layout/footer/footer.component';
import { HeaderComponent } from './private-layout/header/header.component';
import { PrivateLayoutComponent } from './private-layout/private-layout.component';
import { EmptyLayoutComponent } from './empty-layout/empty-layout.component';
import { ErrorForbidenComponent } from './error-layout/error-forbiden/error-forbiden.component';
import { ErrorNotfoundComponent } from './error-layout/error-notfound/error-notfound.component';
import { ErrorServerComponent } from './error-layout/error-server/error-server.component';
import { ErrorAuthorizedComponent } from './error-layout/error-authorized/error-authorized.component';
import { NzIconModule } from 'ng-zorro-antd/icon';



@NgModule({
    declarations: [
        HeaderComponent,
        PrivateLayoutComponent,
        FooterComponent,
        EmptyLayoutComponent,
        ErrorForbidenComponent,
        ErrorNotfoundComponent,
        ErrorServerComponent,
        ErrorAuthorizedComponent,
    ],
    imports: [
        CommonModule,
        RouterOutlet,
        RouterLink,
        NzLayoutModule,
        NzMenuModule,
        NzDropDownModule,
        NzBreadCrumbModule,
        NzIconModule
    ]
})
export class LayoutModule { }
