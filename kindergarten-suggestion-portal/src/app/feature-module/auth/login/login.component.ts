import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  standalone: true,
  imports: [
    ReactiveFormsModule,
    CommonModule
  ],
})
export class LoginComponent implements OnInit {
  // @ViewChild('closeButton')
  // closeButton!: ElementRef<HTMLElement>;

  loginForm!: FormGroup;
  errorMessage!: string;
  token!: string;
  role!: string;
  inputEmailTouched = false;
  inputPasswordTouched = false;
  userData: any;

  constructor(private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _router: Router
  ) { }
  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
      password: ['', Validators.required]
    })
    // Theo dõi sự kiện focus của ô input
    this.email?.valueChanges.subscribe(() => {
      this.inputEmailTouched = true;
    });

    this.password?.valueChanges.subscribe(() => {
      this.inputPasswordTouched = true;
    });
  }
  onSubmit() {
    this.inputEmailTouched = true;
    this.inputPasswordTouched = true;


    this.errorMessage = "";
    if (this.loginForm.invalid) {
      return;
    }
    this._authService.login(this.loginForm.value).subscribe({
      next: response => {
        console.log(" response data :" + response)
        this.token = response.accessToken;
        this.role = response.account.role;
        localStorage.setItem('accessToken', this.token);
        localStorage.setItem('role', this.role)
        this.userData = {
          userId: response.id,
          fullName: response.fullName,
          email: response.email,
          phoneNumber: response.phoneNumber,
          city: response.city,
          district: response.district,
          ward: response.ward,
          street: response.street,
          dOB: new Date(response.dob),
        }
        console.log(response);
        localStorage.setItem('userData', JSON.stringify(this.userData));
        // let el: HTMLElement = this.closeButton.nativeElement;
        // el.click();

        //TODO:Check role
        //admin/foods

        this._router.navigateByUrl('');
        window.location.reload();

      },
      error: error => {
        console.log("😜😜😜 ~ file: login.component.ts:87 ~ LoginComponent ~ this._authService.login ~ error:", error)
        this.errorMessage = "Email or password is invalid!"
      }
    });

  }
  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }
  clearForm() {
    this.loginForm.reset();
    this.errorMessage = '';
    this.inputEmailTouched = false;
    this.inputPasswordTouched = false;
  }
}
