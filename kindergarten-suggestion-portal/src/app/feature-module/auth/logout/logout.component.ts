import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
  standalone: true,
  imports: [
    ReactiveFormsModule,
    CommonModule
  ],
})
export class LogoutComponent {
  constructor(private _authService: AuthService,
              private _router: Router) { }
  logOut(){
    console.log("logout");
    this._authService.logOut().subscribe({
      next: (resp) => {
        localStorage.clear();
        this._router.navigate(['/home']).then(r => window.location.reload());
      },
      error : (error) => {
          // Xử lý lỗi logout, ví dụ: hiển thị thông báo lỗi.
          console.error('Logout failed:', error);
        }
    }
    );
  }
}
