import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { RequestCounsellingService } from "../../../../service/request-counselling/request-counselling.service";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-my-request',
  templateUrl: './my-request.component.html',
  styleUrls: ['./my-request.component.scss']
})
export class MyRequestComponent implements OnInit {
  requestList: any = [];
  totalPages: number = 0;
  totalRequest: number = 0;
  totalRequestOpen: number = 0;
  activePage: number = 1;
  size: number = 0;
  numberOfElements !: number;
  userData: any;
  // userId !: number;
  isClick: boolean = false;
  isChecked: boolean = false;
  @ViewChild('myRequest') myRequest!: ElementRef;
  @ViewChild('cancel') closeBtn!: ElementRef;


  constructor(private _requestService: RequestCounsellingService,
    private _activeRoute: ActivatedRoute,
    private _router: Router,
    private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this._activeRoute.queryParams.subscribe(params => {
      this.activePage = Number(params['page']) + 1 || 1;
      this.size = params['size'] || 3;
    })
    const userDataString = localStorage.getItem('userData');
    if (userDataString) {
      this.userData = JSON.parse(userDataString);
      // this.userId = this.userData.userId;
      this.numberOfElements = this.userData?.numberOfElements || 0;

    }
    this.loadData();

  }


  loadData() {
    this._requestService.getRequestById(this.activePage - 1, this.size).subscribe({
      next: response => {
        if (!response) {
          this.myRequest.nativeElement.innerHTML =
            `
          <p class="text-center">There’s no requests available.</p
        `
        } else {
          console.log(response);
          this.requestList = response.content;
          this.requestList = this.requestList.map((item: any) => ({
            ...item,
            showMore: false,
          }));
          console.log(this.requestList);
          this.totalPages = response.totalPages;
          this.totalRequest = response.totalElements;
          this._requestService.getRequestById(0, this.totalRequest).subscribe({
            next: response => {
              if (!this.isChecked) {
                for (const res of response.content) {
                  if (res.status === 'OPEN') {
                    this.totalRequestOpen++;
                  }
                }
                this.isChecked = true;
              }


            }, error: err => {

            }
          })
        }

      },
      error: err => {
        console.log("😜😜😜 ~ file: my-request.component.ts:80 ~ MyRequestComponent ~ this._requestService.getRequestById ~ err:", err)
        this.myRequest.nativeElement.innerHTML =
          `
          <p class="text-center">There’s no requests available.</p
        `
      }
    })
  }

  paging(i: number) {
    this.activePage = i;
    this.loadData();
  }

  handlePageChange(event: number) {
    this.activePage = event;
    console.log("😜😜😜 ~ file: my-request.component.ts:75 ~ MyRequestComponent ~ handlePageChange ~ this.activePage:", this.activePage)
    const queryParams = { ...this._activeRoute.snapshot.queryParams };
    queryParams['page'] = event - 1;
    queryParams['size'] = this.size;
    this._router.navigate([], {
      relativeTo: this._activeRoute,
      queryParams,
      queryParamsHandling: 'merge',
    }).then(r =>
      this.loadData()
    );

  }

  trimString(text: string, length: number) {
    if (!text) {
      return text;
    }
    return text.length > length ?
      text.substring(0, length) + '...' :
      text;
  }

  cancelRequest(id: number) {
    this._requestService.cancelRequest(id).subscribe({
      next: res => {
        this.closeBtn.nativeElement.click();
        this.showSuccess();
        window.location.reload();
      },
      error: err => {
        this.showFail();
      }
    })
  }

  showSuccess() {
    this.toastr.success('Cancel request successfully!');
  }

  showFail() {
    this.toastr.error('Cancel request failed!');
  }
}
