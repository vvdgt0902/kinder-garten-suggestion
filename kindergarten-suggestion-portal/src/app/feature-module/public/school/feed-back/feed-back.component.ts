import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../../../../service/user.service";
import {finalize} from "rxjs";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-feed-back',
  templateUrl: './feed-back.component.html',
  styleUrls: ['./feed-back.component.scss']
})
export class FeedBackComponent implements OnInit {
  feedBackForm!: FormGroup;
  isSubmittingForm = false;
  selectedId!: number;
  @Input() schoolId : number = 0;
  constructor(private _formBuilder: FormBuilder,
              private _router: Router,
              private _userService: UserService,
              private _toastr: ToastrService,
  ) {
  }

  ngOnInit(): void {
    this.feedBackForm = this._formBuilder.group({
      learningProgramRating: ['', Validators.required],
      facilityAndUtilityRating: ['', Validators.required],
      extracurricularActivityRating: ['', Validators.required],
      teacherAndStaffRating: ['', Validators.required],
      hygieneAndNutritionRating: ['', Validators.required],
      content: [''],
      schoolId: this.schoolId,
      userId: [''],
    });
    const userData = localStorage.getItem('userData');
    if (userData) {
      const parsedUserData = JSON.parse(userData);
      this.feedBackForm.get('userId')?.setValue(parsedUserData.userId);
    }

  }

  submitForm() {
    for (const i in this.feedBackForm.controls) {
      this.feedBackForm.controls[i].markAsDirty();
      this.feedBackForm.controls[i].updateValueAndValidity();
    }
    if (this.feedBackForm.invalid || this.isSubmittingForm) {
      return;
    }
    this.isSubmittingForm = true;

    const feedBackFormPayload = this.feedBackForm.value;
    console.log(feedBackFormPayload);
    if (this.selectedId) {
      // Edit
    } else {
      // Create new
      this._userService.feedBack(feedBackFormPayload)
        .pipe(finalize(() => this.isSubmittingForm = false))
        .subscribe({
          next: res => {
            this.showSuccess();

            setTimeout(() => {
              window.location.reload();
            }, 1000);
          },
          error: err => {
            this.showFail();
          }
        });
    }
  }
  clearForm() {
    this.feedBackForm.reset();
  }
  showSuccess() {
    this._toastr.success('Feed back successfully!');
  }

  showFail() {
    this._toastr.error('Feed back failed!');
  }
  get learningProgramRating() { return this.feedBackForm.get('learningProgramRating'); }
  get facilityAndUtilityRating() { return this.feedBackForm.get('facilityAndUtilityRating'); }
  get extracurricularActivityRating() { return this.feedBackForm.get('extracurricularActivityRating'); }
  get teacherAndStaffRating() { return this.feedBackForm.get('teacherAndStaffRating'); }
  get hygieneAndNutritionRating() { return this.feedBackForm.get('hygieneAndNutritionRating'); }
}
