import {Component, OnInit} from '@angular/core';
import {SchoolService} from "../../../../service/school-service/school.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ImageUtility} from "../../../../environment/displayImage";

@Component({
  selector: 'app-school-enrolled',
  templateUrl: './school-enrolled.component.html',
  styleUrls: ['./school-enrolled.component.scss']
})
export class SchoolEnrolledComponent implements OnInit {
  isCurrent!: any;
  isPrevious!: any;
  status!: boolean;
  activePage: number = 1;
  size: number = 4;
  totalPages!: number;
  listSchool: any[] = [];
  totalElements!: number;
  rating : any;

  constructor(private _schoolService: SchoolService,
              private _activeRoute: ActivatedRoute,
              private _router: Router) {
  }

  ngOnInit(): void {
    this.isCurrent = 'text-primary fw-bold';
    this.isPrevious = 'text-dark';
    this.status = true;
    this.totalElements = 0;
    this._activeRoute.queryParams.subscribe(params => {
      this.activePage = Number(params['page']) + 1 || this.activePage;
      this.size = Number(params['size']) || this.size;
    });
    this.loadEnrolledSchool();
  }

  clickTab(target: any) {
    if (target) {
      this.isCurrent = 'text-dark';
      this.isPrevious = 'text-primary fw-bold';
      this.status = false;
    } else {
      this.isCurrent = 'text-primary fw-bold';
      this.isPrevious = 'text-dark';
      this.status = true;
    }
    this.activePage = 1;
    const queryParams = { ...this._activeRoute.snapshot.queryParams };
    queryParams['page'] = 0;
    queryParams['size'] = this.size;
    this._router.navigate([], {
      relativeTo: this._activeRoute,
      queryParams,
      queryParamsHandling: 'merge',
    }).then(r =>
      this.loadEnrolledSchool()
    );
  }

  loadEnrolledSchool() {
    this._schoolService.getEnrolledSchool(this.activePage - 1, this.size, this.status).subscribe({
      next: resp => {
        this.totalPages = resp.totalPages;
        this.listSchool = resp.content;
        this.totalElements = resp.totalElements;
        console.log(resp);
      }, error: err => {
        console.error(err);
      }
    })
  }

  handlePageChange(event: number) {
    this.activePage = event;
    const queryParams = { ...this._activeRoute.snapshot.queryParams };
    queryParams['page'] = event - 1;
    queryParams['size'] = this.size;
    this._router.navigate([], {
      relativeTo: this._activeRoute,
      queryParams,
      queryParamsHandling: 'merge',
    }).then(r =>
      this.loadEnrolledSchool()
    );
  }

  handleViewRate(id : number) {
    let school = this.listSchool.filter((school : any) => school.school?.feedback?.id === id);
    this.rating = school[0].school.feedback;
    console.log(this.rating);
    console.log(typeof this.rating.createdDate)
  }

  protected readonly ImageUtility = ImageUtility;
}
