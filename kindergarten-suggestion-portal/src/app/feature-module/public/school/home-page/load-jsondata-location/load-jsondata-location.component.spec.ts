import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadJsondataLocationComponent } from './load-jsondata-location.component';

describe('LoadJsondataLocationComponent', () => {
  let component: LoadJsondataLocationComponent;
  let fixture: ComponentFixture<LoadJsondataLocationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoadJsondataLocationComponent]
    });
    fixture = TestBed.createComponent(LoadJsondataLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
