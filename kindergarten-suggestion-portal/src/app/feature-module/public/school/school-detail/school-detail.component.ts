import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ImageUtility } from "../../../../environment/displayImage";
import { environment } from "../../../../environment/environment";
import { SchoolDetailService } from "../../../../service/school-detail.service";

@Component({
  selector: 'app-school-detail',
  templateUrl: './school-detail.component.html',
  styleUrls: ['./school-detail.component.scss']
})
export class SchoolDetailComponent implements OnInit {
  schoolId: any;
  schoolDetail: any = {};
  listFeedbacks: any = [];
  listFeedBackFilter: any = [];
  listFacility: any[] = [];
  listUtility: any[] = [];
  listImage: any[] = [];
  mainImage: any = '';
  activeButton: number | null = null;
  checkEnrolled : boolean = false;
  userId : any;
  // userId : any;
  constructor(
    private _schoolService: SchoolDetailService,
    private _activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe(params => {
      this.schoolId = Number(params['id']);

      const  userData = localStorage.getItem("userData");
      if(userData){
        const getData = JSON.parse(userData)
        this.userId = getData.userId

      }

      this.getDetailSchool();
      this.getListFacility();
      this.getListUtility();
    });
  }


  //lấy data
  getDetailSchool() {
    this._schoolService.getDetailSchool(Number(this.schoolId),Number(this.userId)).subscribe(schoolDetail => {

      this.schoolDetail = schoolDetail;
      this.listFeedbacks = schoolDetail.feedBacks;
      this.listFeedBackFilter = this.listFeedbacks;
      console.log(this.listFeedBackFilter)
      console.log(this.listFeedbacks)
      this.getListImage()
      this.checkEnrolled = schoolDetail.rate;

    });
  }


  //kiểm tra trường có cái nào thì check vào cái đó
  getListFacility() {
    this._schoolService.getListFacility().subscribe(list => {
      this.listFacility = list;
    })
  }

  getListUtility() {
    this._schoolService.getListUtility().subscribe(list => {
      this.listUtility = list
    })
  }

  isCheckedfacility(facilityCode: string): any {
    for (let i = 0; i < this.schoolDetail.schoolFacilities?.length; i++) {
      if (this.schoolDetail.schoolFacilities[i].facility.code === facilityCode) {
        return true;
      }
    }
  }

  isCheckedUtility(UtilityCode: string): any {
    for (let i = 0; i < this.schoolDetail.schoolUtilities?.length; i++) {
      if (this.schoolDetail.schoolUtilities[i].utility.code === UtilityCode) {
        return true;
      }
    }
  }

  getRateDetailNumber(rate: number) {
    if (!rate) {
      this.listFeedBackFilter = this.listFeedbacks;
      this.activeButton = rate;
    } else {
      this.activeButton = rate;
      this.listFeedBackFilter = this.listFeedbacks.filter((fb: any) => fb.avgRating >= rate && fb.avgRating < rate + 1)
    }
  }

  getListImage() {
    this.listImage = [];
    this.mainImage = this.schoolDetail.images.filter((img: any) => img.isMainImage)[0];
    let img = this.schoolDetail.images.filter((img: any) => !img.isMainImage);
    for (let i = 0; i < img.length; i+=3) {
      this.listImage.push([img[i], img[i+1] ? img[i+1] : img[0], img[i+2] ? img[i+2] : img[1]]);
    }
  }

  protected readonly environment = environment;
  protected readonly ImageUtility = ImageUtility;
}

