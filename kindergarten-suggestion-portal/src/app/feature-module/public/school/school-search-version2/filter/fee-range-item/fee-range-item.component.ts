import {Component, AfterViewInit, ElementRef, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {SchoolSearchService} from "../../../../../../service/school-search/school-search.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-fee-range-school-item',
  templateUrl: './fee-range-item.component.html',
  styleUrls: ['./fee-range-item.component.scss']
})
export class FeeRangeItemComponent implements OnInit, AfterViewInit {


  feeForm!: FormGroup;

  constructor(private elementRef: ElementRef,
              private _router: Router,
              private _activeRoute: ActivatedRoute,
              private _fb: FormBuilder) {
  }

  ngOnInit(): void {

    // ... Other code

    const inputLeft = this.elementRef.nativeElement.querySelector("#input-left");
    const inputRight = this.elementRef.nativeElement.querySelector("#input-right");
    const thumbLeft = this.elementRef.nativeElement.querySelector(".slider > .thumb.left");
    const thumbRight = this.elementRef.nativeElement.querySelector(".slider > .thumb.right");

    inputLeft.addEventListener("input", this.setLeftValue.bind(this));
    inputRight.addEventListener("input", this.setRightValue.bind(this));

    inputLeft.addEventListener("mouseover", () => {
      thumbLeft.classList.add("hover");
    });
    inputLeft.addEventListener("mouseout", () => {
      thumbLeft.classList.remove("hover");
    });
    inputLeft.addEventListener("mousedown", () => {
      thumbLeft.classList.add("active");
    });
    inputLeft.addEventListener("mouseup", () => {
      thumbLeft.classList.remove("active");
    });

    inputRight.addEventListener("mouseover", () => {
      thumbRight.classList.add("hover");
    });
    inputRight.addEventListener("mouseout", () => {
      thumbRight.classList.remove("hover");
    });
    inputRight.addEventListener("mousedown", () => {
      thumbRight.classList.add("active");
    });
    inputRight.addEventListener("mouseup", () => {
      thumbRight.classList.remove("active");
    });

    this.setLeftValue();
    this.setRightValue();
  }

  buildUrlCondition(event: any, fieldName: string) {
    this._router.navigate([], {
      queryParams: {
        ...this.feeForm.value
      },
      queryParamsHandling: "merge",
    })
  }


  ngAfterViewInit(): void {


    const inputLeft = this.elementRef.nativeElement.querySelector("#input-left");
    const inputRight = this.elementRef.nativeElement.querySelector("#input-right");
    const minFeeInput = this.elementRef.nativeElement.querySelector("#minFee");
    const maxFeeInput = this.elementRef.nativeElement.querySelector("#maxFee");
    const thumbLeft = this.elementRef.nativeElement.querySelector(".slider > .thumb.left");
    const thumbRight = this.elementRef.nativeElement.querySelector(".slider > .thumb.right");
    const range = this.elementRef.nativeElement.querySelector(".slider > .range");


    inputLeft.addEventListener("input", this.setLeftValue.bind(this));
    inputRight.addEventListener("input", this.setRightValue.bind(this));
    minFeeInput.addEventListener("input", this.updateRangeFromMinFee.bind(this));
    maxFeeInput.addEventListener("input", this.updateRangeFromMaxFee.bind(this));


    // const inputLeft = this.elementRef.nativeElement.querySelector("#input-left");
    // const inputRight = this.elementRef.nativeElement.querySelector("#input-right");
    // const thumbLeft = this.elementRef.nativeElement.querySelector(".slider > .thumb.left");
    // const thumbRight = this.elementRef.nativeElement.querySelector(".slider > .thumb.right");

    inputLeft.addEventListener("input", this.setLeftValue.bind(this));
    inputRight.addEventListener("input", this.setRightValue.bind(this));


    this.setLeftValue();
    this.setRightValue();
  }

  updateRangeFromMinFee() {
    const minFeeInput = this.elementRef.nativeElement.querySelector("#minFee");
    const maxFeeInput = this.elementRef.nativeElement.querySelector("#maxFee");
    const inputLeft = this.elementRef.nativeElement.querySelector("#input-left");
    const inputRight = this.elementRef.nativeElement.querySelector("#input-right");
    const thumbLeft = this.elementRef.nativeElement.querySelector(".slider > .thumb.left");
    const range = this.elementRef.nativeElement.querySelector(".slider > .range");

    const min = parseInt(inputLeft.min);
    const max = parseInt(inputRight.max);

    let minFeeValue = parseInt(minFeeInput.value);
    const maxFeeValue = parseInt(maxFeeInput.value);
    if (minFeeInput.value === '') {
      minFeeValue = 1;
    }
    if (minFeeValue < min) {
      minFeeValue = min;
    } else if (minFeeValue > maxFeeValue) {
      minFeeValue = maxFeeValue;
    }

    inputLeft.value = minFeeValue;
    this.updateRange();

    const percent = ((minFeeValue - min) / (max - min)) * 100;
    thumbLeft.style.left = percent + "%";
    range.style.left = percent + "%";
  }

  updateRangeFromMaxFee() {
    const minFeeInput = this.elementRef.nativeElement.querySelector("#minFee");
    const maxFeeInput = this.elementRef.nativeElement.querySelector("#maxFee");
    const inputLeft = this.elementRef.nativeElement.querySelector("#input-left");
    const inputRight = this.elementRef.nativeElement.querySelector("#input-right");
    const thumbRight = this.elementRef.nativeElement.querySelector(".slider > .thumb.right");
    const range = this.elementRef.nativeElement.querySelector(".slider > .range");

    const min = parseInt(inputLeft.min);
    const max = parseInt(inputRight.max);

    const minFeeValue = parseInt(minFeeInput.value);
    let maxFeeValue = parseInt(maxFeeInput.value);
    if (maxFeeInput.value === '') {
      maxFeeValue = 19;
    }
    if (maxFeeValue > max) {
      maxFeeValue = max;
    } else if (maxFeeValue < minFeeValue) {
      maxFeeValue = minFeeValue;
    }

    inputRight.value = maxFeeValue;
    this.updateRange();

    const percent = ((maxFeeValue - min) / (max - min)) * 100;
    thumbRight.style.right = (100 - percent) + "%";
    range.style.right = (100 - percent) + "%";
  }

  updateRange() {
    // Implement your updateRange function logic here if needed
  }

  setLeftValue() {
    const inputLeft = this.elementRef.nativeElement.querySelector("#input-left");
    const inputRight = this.elementRef.nativeElement.querySelector("#input-right");
    const minValue = this.elementRef.nativeElement.querySelector("#minFee");
    const min = parseInt(inputLeft.min);
    const max = parseInt(inputRight.max);

    inputLeft.value = Math.min(parseInt(inputLeft.value), parseInt(inputRight.value) - 1);
    minValue.value = inputLeft.value = Math.min(parseInt(inputLeft.value), parseInt(inputRight.value) - 1);

    console.log(minValue);


    const percent = ((parseInt(inputLeft.value) - min) / (max - min)) * 100;
    const thumbLeft = this.elementRef.nativeElement.querySelector(".slider > .thumb.left");
    const range = this.elementRef.nativeElement.querySelector(".slider > .range");

    thumbLeft.style.left = percent + "%";
    range.style.left = percent + "%";
  }

  setRightValue() {
    const inputLeft = this.elementRef.nativeElement.querySelector("#input-left");
    const inputRight = this.elementRef.nativeElement.querySelector("#input-right");
    const maxValue = this.elementRef.nativeElement.querySelector("#maxFee");
    const min = parseInt(inputLeft.min);
    const max = parseInt(inputRight.max);

    inputRight.value = Math.max(parseInt(inputRight.value), parseInt(inputLeft.value) + 1);

    maxValue.value = inputRight.value
    const percent = ((parseInt(inputRight.value) - min) / (max - min)) * 100;
    const thumbRight = this.elementRef.nativeElement.querySelector(".slider > .thumb.right");
    const range = this.elementRef.nativeElement.querySelector(".slider > .range");

    thumbRight.style.right = (100 - percent) + "%";
    range.style.right = (100 - percent) + "%";
  }


}
