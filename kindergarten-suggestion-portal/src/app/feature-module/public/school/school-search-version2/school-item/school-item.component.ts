import {Component, Input, OnInit} from '@angular/core';
import {SchoolSearchService} from "../../../../../service/school-search/school-search.service";
import {ActivatedRoute} from "@angular/router";
import {ImageUtility} from "../../../../../environment/displayImage";

@Component({
  selector: 'app-school-item',
  templateUrl: './school-item.component.html',
  styleUrls: ['./school-item.component.scss']
})
export class SchoolItemComponent implements OnInit{
  clicked = false;
  @Input()
  school:any;
  constructor(private _schoolService: SchoolSearchService,
              private _activeRoute : ActivatedRoute) {
  }

  ngOnInit(): void {

  }

  protected readonly Boolean = Boolean;
    protected readonly ImageUtility = ImageUtility;
}
