import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import {CommonModule, NgOptimizedImage} from '@angular/common';

import {SchoolRoutingModule} from './school-routing.module';
import {ListSchoolComponent} from './school-search-version2/list-school/list-school.component';
import {SchoolDetailComponent} from './school-detail/school-detail.component';
import {SchoolEnrolledComponent} from './school-enrolled/school-enrolled.component';
import {NgxPaginationModule} from "ngx-pagination";
import {SchoolItemComponent} from './school-search-version2/school-item/school-item.component';

import {LoadJsondataLocationComponent} from './home-page/load-jsondata-location/load-jsondata-location.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HomePageComponent} from "./home-page/home-page.component";
import {FeeRangeItemComponent} from "./school-search-version2/filter/fee-range-item/fee-range-item.component";
import {SearchComponent} from "./school-search-version2/search/search.component";
import {FilterComponent} from "./school-search-version2/filter/filter.component";
import {RatingStarComponent} from "./rating-star/rating-star.component";
import {ChildReceivingAgeDisplayNamePipe} from "../../../pipe/child-receiving-age-display-name.pipe";
import {SchoolTypePipe} from "../../../pipe/school-type.pipe";
import {ResultListSchoolsPipe} from "../../../pipe/result-list-schools.pipe";
import {FeedBackComponent} from "./feed-back/feed-back.component";
import {StarRatingComponent} from "./feed-back/component/star-rating/star-rating.component";
import {RequestCounsellingComponent} from "./request-counselling/request-counselling.component";


@NgModule({
  declarations: [
    StarRatingComponent,
    LoadJsondataLocationComponent,
    SchoolDetailComponent,
    ListSchoolComponent,
    SchoolItemComponent,
    FeeRangeItemComponent,
    SearchComponent,
    FilterComponent,
    SchoolEnrolledComponent,
    HomePageComponent,
    RequestCounsellingComponent,
    SchoolDetailComponent,
    RatingStarComponent,
    FeedBackComponent,
  ],
  imports: [
    CommonModule,
    SchoolRoutingModule,
    FormsModule,
    ChildReceivingAgeDisplayNamePipe,
    SchoolTypePipe,
    ReactiveFormsModule,
    ResultListSchoolsPipe,
    NgOptimizedImage,
    NgxPaginationModule,
    ToastrModule.forRoot()

  ],
  exports: [RatingStarComponent]
})
export class SchoolModule {
}
