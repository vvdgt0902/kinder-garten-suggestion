import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchoolDetailComponent } from './school-detail/school-detail.component';
import {SchoolEnrolledComponent} from "./school-enrolled/school-enrolled.component";
import {HomePageComponent} from "./home-page/home-page.component";
import {ListSchoolComponent} from "./school-search-version2/list-school/list-school.component";

const routes: Routes = [
  { path: 'detail', component: SchoolDetailComponent },
  {path : '', component: HomePageComponent},
  {path : 'enrolled', component: SchoolEnrolledComponent},
  {path : 'school/:id', component: SchoolDetailComponent},
  {path : 'school', component: ListSchoolComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchoolRoutingModule {
}
