import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { environment } from "../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private _httpClient: HttpClient) { }
  feedBack(formData: any): Observable<any> {
    return this._httpClient.post(`${environment.apiPrefixUrl}/user/feedback`, formData);
  }
  update( formData: any): Observable<any> {
    return this._httpClient.put(`${environment.apiPrefixUrl}/user/update`, formData);
  }
  updatePassword(formData: any): Observable<any> {
    return this._httpClient.put(`${environment.apiPrefixUrl}/account/change-password`, formData);
  }
  getUser(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/user`);
  }
  resetPassword(formData: any): Observable<any> {
    return this._httpClient.put(`${environment.apiPrefixUrl}/account/reset-password`, formData);
  }
  loadResetPage(token:string): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/account/reset-password/${token}`,{responseType:'text'});
  }
}
