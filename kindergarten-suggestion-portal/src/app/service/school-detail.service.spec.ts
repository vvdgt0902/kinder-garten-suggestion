import { TestBed } from '@angular/core/testing';

import { SchoolDetailService } from './school-detail.service';

describe('SchoolDetailService', () => {
  let service: SchoolDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SchoolDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
