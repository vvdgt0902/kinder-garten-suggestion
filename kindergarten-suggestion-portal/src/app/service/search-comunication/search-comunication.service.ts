import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SearchComunicationService {

  constructor() { }

  private searchDataSubject = new BehaviorSubject<any>(null);

  setSearchData(data: any) {

    this.searchDataSubject.next(data);

  }
  getSearchData() {

    return this.searchDataSubject.asObservable();

  }

}
