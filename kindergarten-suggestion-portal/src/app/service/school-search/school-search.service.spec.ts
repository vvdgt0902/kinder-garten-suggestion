import { TestBed } from '@angular/core/testing';

import { SchoolSearchService } from './school-search.service';


describe('SchoolService', () => {
  let service: SchoolSearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SchoolSearchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
