import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class SchoolSearchService {

  constructor(private _httpClient: HttpClient) { }
  getSchoolList(page: number, size: number,searchOptions: any ): Observable<any> {
      let params = new HttpParams()
          .set('page', page.toString())
          .set('size', size.toString());

    if (searchOptions) {
      for (const key in searchOptions) {
        if (searchOptions.hasOwnProperty(key)) {
          if (key === 'facilities' || key === 'utilities') {
            // Xử lý các trường hợp đặc biệt cho 'facilities' và 'utilities' là mảng
            if (searchOptions[key] && Array.isArray(searchOptions[key])) {
              searchOptions[key].forEach((value: string | number | boolean) => {
                params = params.append(key, value);
              });
            }
          } else if (searchOptions[key]) {
            // Xử lý các trường hợp khác
            params = params.set(key, searchOptions[key]);
          }
        }
      }
    }
      return this._httpClient.get(`${environment.apiPrefixUrl}/school/search`, {
      params
    });
  }


  getSchoolTypes(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/SchoolTypes`, {
    });
  }
  getAdmissionAge(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/enum/AdmissionAge`, {
    });
  }
  getFacility(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/facilities`, {
    });
  }
  getUtility(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/utilities`, {
    });
  }

  getMinFee(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/school/min-fee`, {
    });
  }
  getMaxFee(): Observable<any> {
    return this._httpClient.get(`${environment.apiPrefixUrl}/school/max-fee`, {
    });
  }
}
