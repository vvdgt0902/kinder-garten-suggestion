import { HttpClientModule } from "@angular/common/http";
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from "@angular/router";
import { NgxPaginationModule } from "ngx-pagination";
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChangePasswordComponent } from './feature-module/public/account/change-password/change-password.component';
import { ViewAccountComponent } from './feature-module/public/account/view-account/view-account.component';
import { HTTP_INTERCEPTOR_PROVIDERS } from './interceptor';
import { LayoutModule } from './layout/layout.module';
import { SearchOptions } from "./provider/search-options/search-options";
import { SearchResults } from "./provider/search-results/search-results";


@NgModule({
  declarations: [
    AppComponent,
    ViewAccountComponent,
    ChangePasswordComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    NgxPaginationModule,
    RouterModule.forRoot([]),
    
  ],
  providers: [
    HTTP_INTERCEPTOR_PROVIDERS,
    SearchOptions,
    SearchResults],
  bootstrap: [AppComponent]
})
export class AppModule {
}
