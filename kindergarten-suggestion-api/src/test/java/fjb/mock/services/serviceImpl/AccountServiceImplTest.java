package fjb.mock.services.serviceImpl;

import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.entities.Account;
import fjb.mock.repository.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AccountServiceImplTest {
    @Mock
    private AccountRepository accountRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private AccountServiceImpl accountService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void findByID() {
        // Arrange
        Long accountId = 1L;
        Account mockAccount = new Account();
        when(accountRepository.findById(accountId)).thenReturn(Optional.of(mockAccount));

        // Act
        Optional<Account> result = accountService.findByID(accountId);

        // Assert
        assertTrue(result.isPresent());
        assertEquals(mockAccount, result.get());
    }

    @Test
    void checkExistEmail() {
        // Arrange
        String email = "test@example.com";
        when(accountRepository.existsAccountByEmailAndDeletedFalse(email)).thenReturn(true);

        // Act
        boolean result = accountService.checkExistEmail(email);

        // Assert
        assertTrue(result);
    }

    @Test
    void createNew() {
        // Arrange
        Account newAccount = new Account();
        newAccount.setEmail("test@example.com");
        newAccount.setPassword("password");

        when(passwordEncoder.encode("password")).thenReturn("encodedPassword");

        // Act
        accountService.createNew(newAccount);

        // Assert
        assertEquals("encodedPassword", newAccount.getPassword());
        assertEquals(UserRoleEnum.PARENTS, newAccount.getRole());
        assertFalse(newAccount.isDeleted());
        verify(accountRepository, times(1)).save(newAccount);
    }

    @Test
    void findByEmailAndActiveTrueAndDeletedFalse() {
        // Arrange
        String email = "test@example.com";
        Account mockAccount = new Account();
        when(accountRepository.findByEmailAndActiveTrueAndDeletedFalse(email)).thenReturn(Optional.of(mockAccount));

        // Act
        Optional<Account> result = accountService.findByEmailAndActiveTrueAndDeletedFalse(email);

        // Assert
        assertTrue(result.isPresent());
        assertEquals(mockAccount, result.get());
    }
}