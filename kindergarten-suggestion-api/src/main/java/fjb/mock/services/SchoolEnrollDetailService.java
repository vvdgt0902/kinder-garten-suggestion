package fjb.mock.services;

import fjb.mock.model.entities.School;
import fjb.mock.model.entities.SchoolEnrollDetail;
import fjb.mock.model.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface SchoolEnrollDetailService {
    Page<SchoolEnrollDetail> findAllByUserIdAndIsEnrollTrueAndStatusAndDeletedFalse(Long id, Boolean status, Pageable pageable);
    Optional<SchoolEnrollDetail> findByUserIdAndSchoolIdAndDeleteFalse(Long schoolId, Long userId);

    ResponseEntity<?> createEnroll(Long userId, Long schoolId);
    List<SchoolEnrollDetail> findAllByUserIdAndStatusTrueAndDeletedFalse(Long id);

    SchoolEnrollDetail createNewSchoolEnrollDetail(User user, School school);
}