package fjb.mock.services.serviceImpl;

import fjb.mock.model.entities.SchoolEnrollDetail;
import fjb.mock.repository.SchoolEnrollDetailRepository;
import fjb.mock.services.SchoolUserDetailService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SchoolUserDetailServiceImpl implements SchoolUserDetailService {
    private final SchoolEnrollDetailRepository schoolEnrollDetailRepository;

    public SchoolUserDetailServiceImpl(SchoolEnrollDetailRepository schoolEnrollDetailRepository) {
        this.schoolEnrollDetailRepository = schoolEnrollDetailRepository;
    }

    @Override
    public Page<SchoolEnrollDetail> findAllByUserIdAndIsEnrollTrueAndStatusAndDeletedFalse(Long id, Boolean status, Pageable pageable) {
        return schoolEnrollDetailRepository.findAllByUserIdAndStatusAndDeletedFalse(id, status, pageable);
    }

    @Override
    public Optional<SchoolEnrollDetail> findBySchoolIdAndUserIdAndDeletedFalse(Long schoolId, Long userId) {
        return schoolEnrollDetailRepository.findBySchoolIdAndUserIdAndDeletedFalse(schoolId,userId);
    }


}
