package fjb.mock.services.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import fjb.mock.model.appEnum.RequestStatusEnum;
import fjb.mock.model.entities.RequestCounselling;
import fjb.mock.repository.RequestCounsellingRepository;
import fjb.mock.services.RequestCounsellingService;

@Service
public class RequestCounsellingServiceImpl implements RequestCounsellingService {
    private final RequestCounsellingRepository repository;

    public RequestCounsellingServiceImpl(RequestCounsellingRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<RequestCounselling> findAllByUserIdAndStatusInAndDeletedFalse(Long id, List<RequestStatusEnum> statuses) {
        return repository.findAllByUserIdAndStatusInAndDeletedFalse(id, statuses);
    }

    @Override
    public void create(RequestCounselling requestCounselling) {
        requestCounselling.setDeleted(false);
        requestCounselling.setStatus(RequestStatusEnum.OPEN);
        repository.save(requestCounselling);
    }

    @Override
    public Optional<RequestCounselling> findById(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    @Override
    public Page<RequestCounselling> getPageData(String email, Pageable pageable) {
        return repository.findAllByEmailAndDeletedFalse(email, pageable);
    }



    @Override
    public List<RequestCounselling> findAll(Specification<RequestCounselling> spec, Sort sort) {
        Specification<RequestCounselling> undeleted = ((root, query, criteriaBuilder) -> criteriaBuilder
                .equal(root.get("deleted"), false));
        spec = spec == null ? undeleted : spec.and(undeleted);
        return repository.findAll(spec, sort);
    }

    @Override
    public Page<RequestCounselling> findAllRequest(Specification<RequestCounselling> spec, Pageable pageable) {
        Specification<RequestCounselling> undeleted =
                ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("deleted"), false));
        spec = spec == null ? undeleted : spec.and(undeleted);
        return repository.findAll(spec,pageable);
    }

    @Override
    public Page<RequestCounselling> findAll(Specification<RequestCounselling> spec, Pageable pageable) {
        return repository.findAll(spec, pageable);
    }

    @Override
    public void cancelRequest(RequestCounselling requestCounselling) {
        requestCounselling.setStatus(RequestStatusEnum.CANCELLED);
        repository.save(requestCounselling);
    }

    // // test search api
    @Override
    public Page<RequestCounselling> getPageData(Specification<RequestCounselling>
                                                        spec, Pageable pageable) {
        return repository.findAll(spec, pageable);
    }

    @Override
    public Optional<RequestCounselling> findRequestCounsellingByIdAndDeletedFalse(Long id) {
        return repository.findByIdAndDeletedFalse(id);
    }
    @Override
    public void updateStatus(RequestCounselling requestCounselling) {
        requestCounselling.setStatus(RequestStatusEnum.CLOSED);
        repository.save(requestCounselling);
    }
}
