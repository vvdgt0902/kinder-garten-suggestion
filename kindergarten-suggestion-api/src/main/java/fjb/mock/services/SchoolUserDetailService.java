package fjb.mock.services;

import fjb.mock.model.entities.SchoolEnrollDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface SchoolUserDetailService {
    Page<SchoolEnrollDetail> findAllByUserIdAndIsEnrollTrueAndStatusAndDeletedFalse(Long id, Boolean status, Pageable pageable);

    Optional<SchoolEnrollDetail> findBySchoolIdAndUserIdAndDeletedFalse(Long schoolId, Long userId);

}
