package fjb.mock.model.appEnum;

import lombok.Getter;

@Getter
public enum FacilityEnum {
    OUTDOOR_PLAYGROUND("Outdoor playground"),
    CAMERA("Camera"),
    SWIMMING_POOL("Swimming pool"),
    LIBRARY("Library"),
    CAFETERIA("Cafeteria"),
    MUSICAL_ROOM("Musical room"),
    PE_ROOM("PE room"),
    STEM_ROOM("STEM room");

    public final String displayName;

    FacilityEnum(String displayName) {
        this.displayName = displayName;
    }
}
