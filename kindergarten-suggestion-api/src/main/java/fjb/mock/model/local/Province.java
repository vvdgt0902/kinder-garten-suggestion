package fjb.mock.model.local;

import fjb.mock.model.entities.School;
import fjb.mock.model.entities.User;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Data
@Getter
@Setter
public class Province {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer level;
    
    @OneToMany(mappedBy = "province")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<District> districts;


    @OneToMany(mappedBy = "city")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<User> user;

    @OneToMany(mappedBy = "city")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<School> school;
}
