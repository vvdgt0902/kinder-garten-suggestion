package fjb.mock.model.dto.userDTO;

import fjb.mock.model.appEnum.UserRoleEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TokenAccountRoleDTO {
    private String accessToken;
    private UserRoleEnum role;
}
