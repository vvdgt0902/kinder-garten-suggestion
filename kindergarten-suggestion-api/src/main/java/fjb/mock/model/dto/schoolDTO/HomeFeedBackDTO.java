package fjb.mock.model.dto.schoolDTO;

import lombok.Data;


@Data
public class HomeFeedBackDTO {
    private String content;
    private Double avgRating;
    private String fullName;
    private String imageProfile;

//   UserToFeedBackResponseDTO user;
//   SchoolDetailResponseDTO school;
}
