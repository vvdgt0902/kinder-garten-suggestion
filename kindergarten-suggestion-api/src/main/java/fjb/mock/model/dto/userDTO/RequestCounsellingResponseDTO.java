package fjb.mock.model.dto.userDTO;

import java.time.LocalDateTime;

import fjb.mock.model.appEnum.RequestStatusEnum;
import fjb.mock.model.dto.schoolDTO.SchoolRequestCounsellingResponseDTO;
import jakarta.persistence.Column;
import lombok.Data;

@Data
public class RequestCounsellingResponseDTO {
    private Long id;

    private String fullName;

    private String email;

    private String phoneNumber;

    @Column(length = 4000)
    private String inquiry;

    protected String createdBy;

    protected LocalDateTime createdDate;

    protected String lastModifiedBy;

    protected LocalDateTime lastModifiedDate;

    private RequestStatusEnum status;

    private SchoolRequestCounsellingResponseDTO school;

}
