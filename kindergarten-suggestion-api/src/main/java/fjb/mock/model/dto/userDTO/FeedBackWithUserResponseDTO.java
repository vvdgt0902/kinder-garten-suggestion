package fjb.mock.model.dto.userDTO;

import jakarta.validation.constraints.Size;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class FeedBackWithUserResponseDTO {
    private Long id;
    private String content;
    @Size(min = 1, max = 5, message = "Rating from 1 to 5!")
    private Integer learningProgramRating;
    @Size(min = 1, max = 5, message = "Rating from 1 to 5!")
    private Integer facilityAndUtilityRating;
    @Size(min = 1, max = 5, message = "Rating from 1 to 5!")
    private Integer extracurricularActivityRating;
    @Size(min = 1, max = 5, message = "Rating from 1 to 5!")
    private Integer teacherAndStaffRating;
    @Size(min = 1, max = 5, message = "Rating from 1 to 5!")
    private Integer hygieneAndNutritionRating;
    private Double avgRating;

//    @JsonFormat(pattern = DateTimeConstant.DATE_FORMAT_JSON)
    private LocalDateTime createdDate;
    UserToFeedBackResponseDTO user;
}
