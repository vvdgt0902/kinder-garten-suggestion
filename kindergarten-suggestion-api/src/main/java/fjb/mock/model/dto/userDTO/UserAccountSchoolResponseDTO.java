package fjb.mock.model.dto.userDTO;

import fjb.mock.model.dto.schoolDTO.SchoolEnrollDetailDTO;
import lombok.Data;

import java.util.List;

@Data
public class UserAccountSchoolResponseDTO {
    private Long id;
    private String fullName;
    private String phoneNumber;
    private AccountResponseDTO account;
    private List<SchoolEnrollDetailDTO> schoolEnrollDetails;
    private Integer enroll;
}
