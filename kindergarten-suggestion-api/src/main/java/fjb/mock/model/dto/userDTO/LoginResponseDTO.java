package fjb.mock.model.dto.userDTO;

import java.time.LocalDate;

import fjb.mock.model.dto.addressDTO.DistrictResponseDTO;
import fjb.mock.model.dto.addressDTO.ProvinceResponseDTO;
import fjb.mock.model.dto.addressDTO.WardResponseDTO;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponseDTO {
    private String accessToken;
    private Long id;
    private String fullName;
    private String phoneNumber;
    private ProvinceResponseDTO city;
    private DistrictResponseDTO district;
    private WardResponseDTO ward;
    private String street;
    private LocalDate dOB;
    private String imageProfile;
    private AccountResponseDTO account;
}
