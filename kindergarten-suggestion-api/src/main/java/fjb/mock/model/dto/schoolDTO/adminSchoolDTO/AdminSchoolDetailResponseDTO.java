package fjb.mock.model.dto.schoolDTO.adminSchoolDTO;

import fjb.mock.model.appEnum.ChildReceivingAgeEnum;
import fjb.mock.model.appEnum.EducationMethodEnum;
import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.appEnum.SchoolTypeEnum;
import fjb.mock.model.dto.addressDTO.DistrictResponseDTO;
import fjb.mock.model.dto.addressDTO.ProvinceResponseDTO;
import fjb.mock.model.dto.addressDTO.WardResponseDTO;
import fjb.mock.model.dto.schoolDTO.SchoolFacilitiesResponseDTO;
import fjb.mock.model.dto.schoolDTO.SchoolUtilitiesResponseDTO;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

import java.math.BigInteger;
import java.util.List;

@Data
public class AdminSchoolDetailResponseDTO {

    private Long id;
    private Long OwnerId;
    @NotBlank
    private String name;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")
    private String email;
    @NotBlank
    private ProvinceResponseDTO city;
    @NotBlank
    private DistrictResponseDTO district;
    @NotBlank
    private WardResponseDTO ward;
    private String street;

    private String website;
    private BigInteger minFee;
    private BigInteger maxFee;
    @Column(length = 4000)
    private String introduction;

    @NotBlank
    private SchoolTypeEnum type;
    @NotBlank
    private ChildReceivingAgeEnum childReceivingAge;
    @NotBlank
    private EducationMethodEnum educationMethod;
    @NotBlank
    private SchoolStatusEnum status;

//    List<SchoolImageResponseDTO> images;
//    List<FeedBackWithUserResponseDTO> feedBacks;
    List<SchoolFacilitiesResponseDTO> schoolFacilities;
    List<SchoolUtilitiesResponseDTO> schoolUtilities;

}
