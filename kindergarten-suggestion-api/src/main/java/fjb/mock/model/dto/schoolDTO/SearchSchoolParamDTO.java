package fjb.mock.model.dto.schoolDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SearchSchoolParamDTO {

//    them bởi trungld15

    private String name;
    private String city;
    private String district;
    private String ward;
    private String street;

}
