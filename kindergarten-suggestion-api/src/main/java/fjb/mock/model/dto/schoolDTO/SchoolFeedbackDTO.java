package fjb.mock.model.dto.schoolDTO;

import lombok.Data;

@Data
public class SchoolFeedbackDTO {
    private Double avgLearningProgramRating;
    private Double avgFacilityAndUtilityRating;
    private Double avgExtracurricularActivityRating;
    private Double avgTeacherAndStaffRating;
    private Double avgHygieneAndNutritionRating;
    private Double avgRating;
    private Integer feedbackCount;

}
