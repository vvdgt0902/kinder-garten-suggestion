package fjb.mock.model.entities.enumEntity;

import fjb.mock.model.appEnum.SchoolStatusEnum;
import fjb.mock.model.entities.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
@Entity
public class SchoolStatus extends BaseEntity {
    @Enumerated(EnumType.STRING)
    private SchoolStatusEnum code;
    private String name;
}
