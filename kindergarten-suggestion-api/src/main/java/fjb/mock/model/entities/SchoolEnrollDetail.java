package fjb.mock.model.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
public class SchoolEnrollDetail extends BaseEntity {
    private Boolean status; // Enrolled_True - UnRolled_False - Null_không phải Parents thì không  set giá trị cho biến này.
    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private School school;
    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private User user;
}
