package fjb.mock.repository;

import fjb.mock.model.appEnum.UserRoleEnum;
import fjb.mock.model.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends BaseRepository<User, Long> {

    List<User> findAllByAccount_RoleAndDeletedFalse(UserRoleEnum role);

    Optional<User> findUserByAccountEmailAndDeletedFalse(String email);

    Optional<User> findUserByAccount_RoleAndDeletedFalse(UserRoleEnum role);

    Optional<User> findUserAndAccountByAccountEmailAndDeletedFalse(String email);

    Optional<User> findByIdAndDeletedFalse(Long id);

    Optional<User> findByPhoneNumber(String phoneNumber);

    boolean existsByPhoneNumberAndDeletedFalse(String phoneNumber);

    @Query("SELECT u FROM User u " +
            "JOIN FETCH u.account a " +
            "WHERE a.role = 'PARENTS' " +
            "AND u.deleted = false " +
            "AND a.deleted = false " +
            "AND (:keyword IS NULL " +
            "OR u.fullName LIKE CONCAT('%', :keyword, '%')" +
            "OR u.phoneNumber LIKE CONCAT('%', :keyword, '%')" +
            "OR a.email LIKE CONCAT('%', :keyword, '%'))")
    Page<User> findParentsByAdmin(@Param("keyword") String keyword,
                                  Pageable pageable);
    @Query("SELECT DISTINCT u FROM User u " +
            "JOIN FETCH u.account a " +
            "JOIN FETCH u.schoolEnrollDetails s " +
            "WHERE a.role = 'PARENTS' " +
            "AND u.deleted = false AND a.deleted = false " +
            "AND s.deleted = false AND s.status = true " +
            "AND s.school.id IN " +
            "(SELECT sm.school.id FROM User e " +
            "JOIN e.schoolManagements sm " +
            "WHERE e.id = :userId AND sm.deleted = false)" +
            "AND (:keyword IS NULL " +
            "OR u.fullName LIKE CONCAT('%', :keyword, '%')" +
            "OR u.phoneNumber LIKE CONCAT('%', :keyword, '%')" +
            "OR a.email LIKE CONCAT('%', :keyword, '%'))")
    Page<User> findParentsByOwnerSchool(@Param("userId") Long userId,
                                        @Param("keyword")String keyword,
                                        Pageable pageable);


}
