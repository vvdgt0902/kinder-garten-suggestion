package fjb.mock.repository;

import fjb.mock.model.entities.enumEntity.Utility;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UtilitiesRepository extends BaseRepository<Utility, Long> {
    List<Utility> findAllByDeletedFalse();

    Optional<Utility> findByIdAndDeletedFalse(Long id);
}
