package fjb.mock.repository;

import fjb.mock.model.appEnum.RequestStatusEnum;
import fjb.mock.model.entities.RequestCounselling;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RequestCounsellingRepository extends BaseRepository<RequestCounselling, Long> {

    List<RequestCounselling> findAllByUserIdAndStatusInAndDeletedFalse(Long id, List<RequestStatusEnum> statuses);
    Page<RequestCounselling> findAllByEmailAndDeletedFalse(String email, Pageable pageable);

    Page<RequestCounselling> findAll(Specification<RequestCounselling> spec, Pageable pageable);

    Optional<RequestCounselling> findByIdAndDeletedFalse(Long id);
}