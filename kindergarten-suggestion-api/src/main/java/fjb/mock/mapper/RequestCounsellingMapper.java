package fjb.mock.mapper;

import fjb.mock.model.dto.userDTO.RequestCounsellingDetailResponseDTO;
import fjb.mock.model.dto.userDTO.RequestCounsellingResponseDTO;
import fjb.mock.model.entities.RequestCounselling;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface RequestCounsellingMapper {

    RequestCounsellingResponseDTO toDTO(RequestCounselling requestCounselling);
    RequestCounsellingDetailResponseDTO convertDTO(RequestCounselling requestCounselling);
}
