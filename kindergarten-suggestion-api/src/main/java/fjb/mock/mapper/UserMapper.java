package fjb.mock.mapper;

import fjb.mock.model.dto.userDTO.LoginResponseDTO;
import fjb.mock.model.dto.userDTO.UserAccountSchoolResponseDTO;
import fjb.mock.model.dto.userDTO.UserProfileDTO;
import fjb.mock.model.dto.userDTO.UserResponseDTO;
import fjb.mock.model.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserMapper {
    LoginResponseDTO toDTO(User user);

    User toEntity(UserProfileDTO user);

    UserProfileDTO toProfileDTO(User user);

    UserResponseDTO toUserDTO(User user);

    UserAccountSchoolResponseDTO toUserSchool(User user);
}
