package fjb.mock.resource.publicResource;

import java.util.Optional;

import fjb.mock.security.SecurityUtil;
import fjb.mock.services.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import fjb.mock.model.dto.userDTO.ResetPasswordRequestDTO;
import fjb.mock.model.dto.userDTO.ChangeUserPasswordRequestDTO;
import fjb.mock.model.entities.Account;
import fjb.mock.security.TokenProvider;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/account")
public class AccountResource {

    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;
    private final TokenProvider tokenProvider;

    public AccountResource(AccountService accountService, PasswordEncoder passwordEncoder,
            TokenProvider tokenProvider) {
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
    }


    @PutMapping("/change-password")
    private ResponseEntity<?> updatePassword(
            @Valid @RequestBody ChangeUserPasswordRequestDTO changeUserPasswordRequestDTO) {
        String mail = SecurityUtil.getCurrentAccount();
        Optional<Account> accountOptional = accountService.findByEmailAndActiveTrueAndDeletedFalse(mail);
        if (accountOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        Account account = accountOptional.get();
        if (!passwordEncoder.matches(changeUserPasswordRequestDTO.getCurrentPassword(), account.getPassword())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body("Current password is incorrect");
        }
        account.setPassword(changeUserPasswordRequestDTO.getNewPassword());
        accountService.createNew(account);
        return new ResponseEntity<>(HttpStatus.CREATED);

    }

    @PutMapping("/active/{activeToken}")
    private ResponseEntity<?> activeAccount(@PathVariable String activeToken) {
        if (activeToken == null || activeToken.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        String email = tokenProvider.getEmailToActiveAccount(activeToken);
        Optional<Account> accountOptional = accountService.findByEmailAndActiveFalseAndDeletedFalse(email);
        if (accountOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        accountOptional.get().setActive(true);
        accountService.activeAccount(accountOptional.get());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @GetMapping("/reset-password/{resetToken}")
    private  ResponseEntity<?> resetPassword(@PathVariable String resetToken){
        if (resetToken == null || resetToken.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        String email = tokenProvider.getEmailToResetPassword(resetToken,false);
        if (email==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok("OK");
    }
    @PutMapping("/reset-password")
    private ResponseEntity<?> resetPassword(@RequestBody @Valid ResetPasswordRequestDTO requestDTO) {
        if (requestDTO.getToken() == null || requestDTO.getToken().isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        String email = tokenProvider.getEmailToResetPassword(requestDTO.getToken(),true);
        Optional<Account> accountOptional = accountService.findByEmailAndActiveTrueAndDeletedFalse(email);
        if (accountOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        accountOptional.get().setPassword(requestDTO.getPassword());
        accountService.update(accountOptional.get());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
