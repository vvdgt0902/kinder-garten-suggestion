package fjb.mock.resource.publicResource;

import fjb.mock.mapper.SchoolMapper;
import fjb.mock.model.dto.schoolDTO.FacilityResponseDTO;
import fjb.mock.model.dto.schoolDTO.UtilityResponseDTO;
import fjb.mock.model.entities.enumEntity.Facility;
import fjb.mock.model.entities.enumEntity.Utility;
import fjb.mock.services.FacilitiesService;
import fjb.mock.services.UtilitiesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
    public class FacilitiesAndUtilitiesResource {
    private final FacilitiesService facilitiesService;
    private final UtilitiesService utilitiesService;

    @Autowired
    private final SchoolMapper mapper;

    public FacilitiesAndUtilitiesResource(FacilitiesService facilitiesService, UtilitiesService utilitiesService, SchoolMapper mapper) {
        this.facilitiesService = facilitiesService;
        this.utilitiesService = utilitiesService;
        this.mapper = mapper;
    }


    @GetMapping("/facilities")
    public ResponseEntity<?> getFacilities() {

        List<Facility> facilityList = facilitiesService.findAllByDeletedFalse();
        if(facilityList.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        List<FacilityResponseDTO> FacilitiesDTOS = facilityList.stream().map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(FacilitiesDTOS);
    }
    @GetMapping("/utilities")
    public ResponseEntity<?> getUtilities() {

        List<Utility> utilityList = utilitiesService.findAllByDeletedFalse();
        if(utilityList.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        List<UtilityResponseDTO> utilitiesDTOS = utilityList.stream().map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(utilitiesDTOS);
    }
}
