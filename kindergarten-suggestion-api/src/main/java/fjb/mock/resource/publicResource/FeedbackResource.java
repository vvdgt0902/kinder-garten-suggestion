package fjb.mock.resource.publicResource;

import fjb.mock.mapper.FeedBackMapper;
import fjb.mock.model.dto.userDTO.FeedbackResponseDTO;
import fjb.mock.model.entities.*;
import fjb.mock.services.FeedBackService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/feedback")
public class FeedbackResource {
    private final FeedBackService feedbackService;
    private final FeedBackMapper feedBackMapper;

    public FeedbackResource(FeedBackService feedbackService, FeedBackMapper feedBackMapper) {
        this.feedbackService = feedbackService;
        this.feedBackMapper = feedBackMapper;
    }

    @GetMapping("/top4")
    public ResponseEntity<?> getTopFeedbacks() {
        List<FeedBack> feedBacks = feedbackService.getTopFeedback();
        List<FeedbackResponseDTO> dto = feedBacks.stream()
                .map(feedBackMapper::toDTO).toList();
        return ResponseEntity.ok(dto);
    }

//    @GetMapping("/{id}")
//    public ResponseEntity<?> getAllFeedbacksSchool(@PathVariable Long id,
//                                                   @RequestParam(defaultValue = "0") Integer page,
//                                                   @RequestParam(defaultValue = "3") Integer size,
//                                                   @RequestParam(required = false, name = "sort") Integer sortBy) {
////        Specification<FeedBack> spec = null;
//        Pageable pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdDate"));
//
//        Page<FeedBack> feedBacks = feedbackService.findAllBySchoolIdAndDeletedFalse(id, pageRequest);
//        Page<FeedBackWithUserResponseDTO> result = feedBacks
//                    .map(feedBackMapper::toDTOs);
//        return ResponseEntity.ok(result);
//    }
}

