package fjb.mock.resource.adminResource;

import fjb.mock.mapper.FeedBackMapper;
import fjb.mock.model.dto.userDTO.FeedbackResponseDTO;
import fjb.mock.model.entities.FeedBack;
import fjb.mock.services.FeedBackService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/admin/feedback")
public class AdminFeedbackResource {
    private final FeedBackService feedbackService;

    private final FeedBackMapper feedBackMapper;

    public AdminFeedbackResource(FeedBackService feedbackService, FeedBackMapper feedBackMapper) {
        this.feedbackService = feedbackService;
        this.feedBackMapper = feedBackMapper;
    }

    @GetMapping("")
    public ResponseEntity<?> getFeedbackByUserIdAndSchoolId(@PathVariable Long SchoolId,
                                                            @PathVariable Long UserId) {
        Optional<FeedBack> feedBackOptional = feedbackService.findBySchoolIdAndUserId(SchoolId, UserId);
        if (feedBackOptional.isEmpty()) {
            ResponseEntity.noContent().build();
        }
        FeedbackResponseDTO feedbackResponseDTO = feedBackMapper.toDTO(feedBackOptional.get());
        return ResponseEntity.ok(feedbackResponseDTO);
    }
}
