package fjb.mock.resource.adminResource;

import fjb.mock.model.dto.schoolDTO.adminSchoolDTO.AdminSchoolRequestDTO;
import fjb.mock.model.entities.School;
import fjb.mock.model.entities.SchoolFacilities;
import fjb.mock.model.entities.SchoolUtilities;
import fjb.mock.model.entities.enumEntity.Facility;
import fjb.mock.model.entities.enumEntity.Utility;
import fjb.mock.services.FacilitiesService;
import fjb.mock.services.UtilitiesService;

import java.util.List;

public class BaseResource {
    private final UtilitiesService utilitiesService;
    private final FacilitiesService facilitiesService;

    public BaseResource(UtilitiesService utilitiesService, FacilitiesService facilitiesService) {
        this.utilitiesService = utilitiesService;
        this.facilitiesService = facilitiesService;
    }

    protected void setSchoolUtilities(School school, AdminSchoolRequestDTO schoolRequest) {
        if (schoolRequest.getSchoolUtilitiesId() != null) {
            List<SchoolUtilities> schoolUtilitiesList =
                    schoolRequest.getSchoolUtilitiesId()
                            .stream().map(utilityId -> {
                                SchoolUtilities schoolUtilities = new SchoolUtilities();
                                schoolUtilities.setSchool(school);
                                Utility utility = utilitiesService.findById(utilityId);
                                schoolUtilities.setUtility(utility);
                                schoolUtilities.setDeleted(false);
                                return schoolUtilities;
                            }).toList();
            school.setSchoolUtilities(schoolUtilitiesList);
        }
    }
    protected void setSchoolFacilities(School school, AdminSchoolRequestDTO schoolRequest) {
        if (schoolRequest.getSchoolFacilitiesId() != null) {
            List<SchoolFacilities> schoolFacilitiesList =
                    schoolRequest.getSchoolFacilitiesId()
                            .stream().map(facilityId -> {
                                SchoolFacilities schoolFacilities = new SchoolFacilities();
                                schoolFacilities.setSchool(school);
                                Facility facility = facilitiesService.findById(facilityId);
                                schoolFacilities.setFacility(facility);
                                schoolFacilities.setDeleted(false);
                                return schoolFacilities;
                            }).toList();
            school.setSchoolFacilities(schoolFacilitiesList);
        }
    }
    protected void copyUnchangedProperties(School school, School oldSchool) {
        school.setSchoolManagement(oldSchool.getSchoolManagement());
        school.setCreatedBy(oldSchool.getCreatedBy());
        school.setCreatedDate(oldSchool.getCreatedDate());
        school.setAvgRating(oldSchool.getAvgRating());
    }
}
